from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
import profiles.urls
import accounts.urls
import api.urls
import dashboard.urls
import web.urls
from . import views

urlpatterns = [
    url(r'^$', views.LandingPage.as_view(), name='home'),
    url(r'^', include(web.urls, namespace='web')),
    url(r'^', include('password_reset.urls')),
    url(r'^dashboard/', include(dashboard.urls, namespace='dashboard')),
    url(r'^about/$', views.AboutPage.as_view(), name='about'),
    url(r'^acerca/$', views.AcercaPage.as_view(), name='acerca'),
    url(r'^api/', include(api.urls)),
    # url(r'^docs/', include('rest_framework_swagger.urls')),
    url(r'^users/', include(profiles.urls, namespace='profiles')),
    url(r'^quiickly-admin/', include(admin.site.urls)),
    url(r'^', include(accounts.urls, namespace='accounts')),
    url(r'^autocomplete/', include('autocomplete_light.urls')),
]

# User-uploaded files like profile pics need to be served in development
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
