"""
Django settings for quiickly project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""
from django.core.urlresolvers import reverse_lazy
from os.path import dirname, join, exists

# Build paths inside the project like this: join(BASE_DIR, "directory")
BASE_DIR = dirname(dirname(dirname(__file__)))
STATICFILES_DIRS = [join(BASE_DIR, 'static')]

ADMINS = (
    ('Rafael Martinez', 'rafael.antonio.martinezs@gmail.com'),
    ('Rafael quiickly', 'rafaelmartinezs@quiickly.co'),
)

DEFAULT_FROM_EMAIL = 'soporte@quiickly.co'
SERVER_EMAIL = 'soporte@quiickly.co'

MEDIA_URL = "/media/"

# Use Django templates using the new Django 1.8 TEMPLATES settings
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            join(BASE_DIR, 'templates'),
            # insert more TEMPLATE_DIRS here
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.core.context_processors.request',
                'django.contrib.messages.context_processors.messages',
                'dashboard.context_processors.menu',
                'web.context_processors.menuWeb',
                'django.core.context_processors.csrf',
            ],
        },
    },
]

PAYU_INFO = {
    'api_key': "6u39nqhq8ftd0hlvnjfs66eh8c",
    'merchantId': '500238',
    'accountId': '500538',
    'currency': 'COP',

    'public_key': "PK64h4gfW9L16b89e3B8RxCTUd",
    # for production environment use 'https://secure.payu.in/_payment'
    'server_produccion': 'https://gateway.payulatam.com/ppp-web-gateway',
    'server_pruebas': 'https://stg.gateway.payulatam.com/ppp-web-gateway',
    'surl_prueba': 'http://54.187.26.141:8000/orden/',
    'curl_prueba': 'http://54.187.26.141:8000/confirmacion/',
    'furl': 'http://example.com/failure/',
    'curl': 'http://example.com/cancel/',
}

# Use 12factor inspired environment variables or from a file
import environ
env = environ.Env()

# Ideally move env file should be outside the git repo
# i.e. BASE_DIR.parent.parent
env_file = join(dirname(__file__), 'local.env')
if exists(env_file):
    environ.Env.read_env(str(env_file))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/dev/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
# Raises ImproperlyConfigured exception if SECRET_KEY not in os.environ
SECRET_KEY = env('SECRET_KEY')


# Application definition

INSTALLED_APPS = (
    'django.contrib.auth',
    'flat',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',

    'authtools',
    'password_reset',
    'crispy_forms',
    'easy_thumbnails',

    'profiles',
    'accounts',
    'dashboard',
    'servicios',
    'notificacion',
    'config',
    'web',
    'suscripcion',
    'payu',

    'django_mailgun',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_framework_swagger',
    'django_filters',
    'django_extensions',
    'widget_tweaks',
    'push_notifications',
    'solo',
    'cities_light',
    'autocomplete_light',

)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

PUSH_NOTIFICATIONS_SETTINGS = {
    "GCM_API_KEY": "AIzaSyA851MR_xbuFkKhRgCdh6R5lrl0H9MWfXk",
    # "APNS_CERTIFICATE": "/path/to/your/certificate.pem",
}

ROOT_URLCONF = 'quiickly.urls'

WSGI_APPLICATION = 'quiickly.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/dev/topics/i18n/

LANGUAGE_CODE = 'es-CO'

TIME_ZONE = 'America/Bogota'

USE_I18N = True

USE_L10N = False

USE_TZ = False

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/dev/howto/static-files/

STATIC_URL = '/static/'

CORREO_ENVIO = 'soporte@quiickly.co'

ADMINS = (
    ('Rafael Martinez', 'rafaelmartinezs@live.com'),
    ('Rafael quiickly', 'rafaelmartinezs@quiickly.co'),
)


# Crispy Form Theme - Bootstrap 3
CRISPY_TEMPLATE_PACK = 'bootstrap3'

# For Bootstrap 3, change error alert to 'danger'
from django.contrib import messages
MESSAGE_TAGS = {
    messages.ERROR: 'danger'
}

# Authentication Settings
AUTH_USER_MODEL = 'authtools.User'
LOGIN_REDIRECT_URL = reverse_lazy("home")
LOGIN_URL = reverse_lazy("accounts:login")

# EMAIL_BACKEND = 'django_mailgun.MailgunBackend'
# MAILGUN_ACCESS_KEY = 'key-1d5af07f47f5859f7037325b56eb77d1'
# MAILGUN_SERVER_NAME = 'mg.script.com.co'

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.mailgun.org'
EMAIL_HOST_USER = 'postmaster@mg.script.com.co'
EMAIL_HOST_PASSWORD = '6fd3925f846edea26cf220bff91db202'
EMAIL_PORT = 587
SERVER_EMAIL = 'soporte@quiickly.co'

THUMBNAIL_EXTENSION = 'png'     # Or any extn for your thumbnails
REST_FRAMEWORK = {
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.

    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ],
    'DEFAULT_AUTHENTICATION_CLASSES': (
        #'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.TokenAuthentication',
    )

}


CORS_ORIGIN_ALLOW_ALL = True
