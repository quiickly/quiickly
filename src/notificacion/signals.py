from django.db.models.signals import pre_save, pre_delete, post_save, post_delete
from django.dispatch import receiver
from push_notifications.models import GCMDevice
from notificacion.models import NotificacionServicio


@receiver(post_save, sender=NotificacionServicio)
def pedido_post_save(sender, instance, created, **kwargs):
    print "############# NOTIFICACION SERVICIO POST SAVE ###################"
    if instance:
        servicio = instance.servicio
        notificaciones = NotificacionServicio.objects.filter(
            servicio=servicio
        )
        cantidad_notificaciones = len(list(notificaciones))
        rechazadas = notificaciones.filter(estado='rechazado').count()
        if cantidad_notificaciones == rechazadas:
            servicio.estado = 'rechazado'
            servicio.save()
            devices_user = GCMDevice.objects.filter(
                user=instance.servicio.user
            )
            devices_user.send_message(
                None,
                extra={
                    "servicio_id": "%s" % instance.servicio.pk,
                    "mensaje": "Tu pedido fue rechazado!.",
                    "tipo": "servicio",
                    "estado": "%s" % instance.servicio.estado
                }
            )
