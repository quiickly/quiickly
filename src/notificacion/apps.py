from __future__ import unicode_literals
from django.apps import AppConfig


class NotificacionConfig(AppConfig):
    name = "notificacion"
    verbose_name = 'Envio de notificacion'

    def ready(self):
        from . import signals