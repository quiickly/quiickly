# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('dashboard', '0019_auto_20151023_1210'),
        ('servicios', '0007_servicio_gratis'),
    ]

    operations = [
        migrations.CreateModel(
            name='NotificacionServicio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('estado', models.CharField(max_length=50, choices=[(b'esperando', b'Esperando confirmacion'), (b'rechazado', b'Rechazada'), (b'tomado', b'Servicio tomado')])),
                ('mensaje', models.CharField(max_length=70, null=True, blank=True)),
                ('quiickler', models.ForeignKey(to='dashboard.Quiickler')),
                ('servicio', models.ForeignKey(to='servicios.Servicio')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
