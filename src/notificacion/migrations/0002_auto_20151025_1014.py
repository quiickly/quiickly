# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notificacion', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notificacionservicio',
            name='estado',
            field=models.CharField(default=b'esperando', max_length=50, choices=[(b'esperando', b'Esperando confirmacion'), (b'rechazado', b'Rechazada'), (b'tomado', b'Servicio tomado')]),
        ),
    ]
