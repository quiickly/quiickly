from django.db import models
from django.conf import settings
from dashboard.models import TimeStampedModel, Quiickler
from servicios.models import Servicio

OPCIONES_ESTADO = (
    ('esperando', 'Esperando confirmacion'),
    ('rechazado', 'Rechazada'),
    ('tomado', 'Servicio tomado')
)


class NotificacionServicio(TimeStampedModel):
    """ Modelo para las notifiaciones
    cuando se le envia la notificacion al quiickler sobre un nuevo pedido
    se creara un registro de este modelo
    con el fin de saber si todos los quiicklers rechazan el Servicio
    y asi poder informar al usuario
    """

    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    servicio = models.ForeignKey(Servicio)
    quiickler = models.ForeignKey(Quiickler)
    estado = models.CharField(
        max_length=50,
        choices=OPCIONES_ESTADO,
        default='esperando'
    )
    mensaje = models.CharField(max_length=70, null=True, blank=True)

    def __unicode__(self):
        return 'Notificacion - servicio: %s - quickler: %s - user: %s' % (
            self.servicio.estado,
            self.user.email,
            self.quiickler.nombre
        )
