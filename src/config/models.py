from django.db import models

from solo.models import SingletonModel


class Configuration(SingletonModel):
    area_busqueda = models.IntegerField(default="5")

    def __unicode__(self):
        return u"Site Configuration"

    class Meta:
        verbose_name = "Variables de configuracion"
