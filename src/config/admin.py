from django.contrib import admin
from .models import Configuration
from solo.admin import SingletonModelAdmin

admin.site.register(Configuration, SingletonModelAdmin)
