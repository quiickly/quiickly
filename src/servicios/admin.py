from django.contrib import admin
from .models import *


@admin.register(Servicio, Pago, Venta, Evaluacion)
class ServicioAdmin(admin.ModelAdmin):
    pass
