# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0019_auto_20151023_1210'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('servicios', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Evaluacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('evaluacion', models.CharField(max_length=15, verbose_name=b'Estado', choices=[(b'excelente', b'Excelente'), (b'bueno', b'Bueno'), (b'regular', b'Regular'), (b'malo', b'Malo')])),
                ('quiickler', models.ForeignKey(to='dashboard.Quiickler')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Pago',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('estado', models.BooleanField(default=False)),
                ('quiickler', models.ForeignKey(related_name='pagos', to='dashboard.Quiickler')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Venta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('total', models.FloatField(null=True)),
                ('quiickler', models.ForeignKey(to='dashboard.Quiickler')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterField(
            model_name='servicio',
            name='estado',
            field=models.CharField(default=b'confirmando', max_length=15, verbose_name=b'Estado', choices=[(b'espera', b'En espera'), (b'confirmando', b'Confirmando con quiickler'), (b'entregado', b'Entregado'), (b'noentregado', b'No entregado'), (b'rechazado', b'Rechazado por los quiicklers')]),
        ),
        migrations.AddField(
            model_name='venta',
            name='servicio',
            field=models.OneToOneField(to='servicios.Servicio'),
        ),
        migrations.AddField(
            model_name='pago',
            name='servicio',
            field=models.OneToOneField(to='servicios.Servicio'),
        ),
        migrations.AddField(
            model_name='evaluacion',
            name='servicio',
            field=models.OneToOneField(to='servicios.Servicio'),
        ),
        migrations.AddField(
            model_name='evaluacion',
            name='user',
            field=models.ForeignKey(related_name='evaluaciones', to=settings.AUTH_USER_MODEL),
        ),
    ]
