# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('dashboard', '0019_auto_20151023_1210'),
        ('servicios', '0002_auto_20151023_1210'),
    ]

    operations = [
        migrations.AddField(
            model_name='servicio',
            name='direccion',
            field=models.ForeignKey(to='dashboard.Direccion', null=True),
        ),
        migrations.AddField(
            model_name='servicio',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True),
        ),
    ]
