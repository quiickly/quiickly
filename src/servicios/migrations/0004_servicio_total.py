# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('servicios', '0003_auto_20151023_1251'),
    ]

    operations = [
        migrations.AddField(
            model_name='servicio',
            name='total',
            field=models.FloatField(null=True),
        ),
    ]
