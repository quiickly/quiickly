# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0019_auto_20151023_1210'),
        ('servicios', '0005_auto_20151023_1338'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='servicio',
            name='quiickler',
        ),
        migrations.AddField(
            model_name='servicio',
            name='quiickler',
            field=models.ForeignKey(to='dashboard.Quiickler', null=True),
        ),
    ]
