# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('servicios', '0009_evaluacion_comentario'),
    ]

    operations = [
        migrations.AddField(
            model_name='venta',
            name='gratis',
            field=models.BooleanField(default=False),
        ),
    ]
