# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0018_auto_20151023_1133'),
    ]

    operations = [
        migrations.CreateModel(
            name='Servicio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('fecha_confirmacion', models.DateTimeField()),
                ('fecha_entrega', models.DateTimeField()),
                ('fecha_programada', models.DateTimeField()),
                ('estado', models.CharField(default=b'espera', max_length=15, verbose_name=b'Estado', choices=[(b'espera', b'En espera'), (b'confirmando', b'Confirmando con quiickler'), (b'entregado', b'Entregado'), (b'noentregado', b'No entregado'), (b'rechazado', b'Rechazado por los quiicklers')])),
                ('tipo', models.CharField(max_length=15, verbose_name=b'Tipo de servicio', choices=[(b'express', b'Express'), (b'programado', b'Programado')])),
                ('longitud', models.FloatField(verbose_name=b'longitud')),
                ('latitud', models.FloatField(verbose_name=b'latitud')),
                ('pedido', models.OneToOneField(related_name='servicios', to='dashboard.Pedido')),
                ('quiickler', models.ManyToManyField(to='dashboard.Quiickler')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
