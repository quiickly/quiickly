# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('servicios', '0007_servicio_gratis'),
    ]

    operations = [
        migrations.AlterField(
            model_name='servicio',
            name='estado',
            field=models.CharField(default=b'confirmando', max_length=15, verbose_name=b'Estado', choices=[(b'espera', b'En espera'), (b'confirmando', b'Confirmando con quiickler'), (b'entregado', b'Entregado'), (b'noentregado', b'No entregado'), (b'rechazado', b'Rechazado por los quiicklers'), (b'sinrespuesta', b'Sin respuesta de los quiicklers')]),
        ),
    ]
