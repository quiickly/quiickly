# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('servicios', '0008_auto_20151102_1347'),
    ]

    operations = [
        migrations.AddField(
            model_name='evaluacion',
            name='comentario',
            field=models.TextField(null=True, blank=True),
        ),
    ]
