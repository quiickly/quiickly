# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('servicios', '0004_servicio_total'),
    ]

    operations = [
        migrations.AlterField(
            model_name='servicio',
            name='fecha_confirmacion',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='servicio',
            name='fecha_entrega',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='servicio',
            name='fecha_programada',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
