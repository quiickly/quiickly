from __future__ import unicode_literals
from django.apps import AppConfig


class ServicioConfig(AppConfig):
    name = "servicios"
    verbose_name = 'Servicios'

    def ready(self):
        from . import signals   # noqa