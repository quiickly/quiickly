from django.db.models.signals import pre_save, pre_delete, post_save, post_delete
from django.dispatch import receiver
from dashboard.haversine import getBoundaries
from push_notifications.models import GCMDevice
from config.models import Configuration
from notificacion.models import NotificacionServicio
from servicios.models import Servicio, Venta, Pago
from dashboard.busqueda import BusquedaQuiicklers


@receiver(post_save, sender=Servicio)
def pedido_post_save(sender, instance, created, **kwargs):
    print "#######################################"
    print "Creado: ", created
    print "Instance: ", instance
    print "ProductoPedido: ", instance.pedido.productopedido_set.all()

    if created:
        lat = float(instance.latitud)
        lng = float(instance.longitud)
        config = Configuration.objects.get()
        distance = config.area_busqueda
        print distance
        quiickler = BusquedaQuiicklers(lat, lng, distance)
        # box = getBoundaries(lat, lng, distance)
        # query = "select *,(6371 * ACOS(COS( RADIANS(%f))*COS(RADIANS(latitud))*COS(RADIANS(longitud)-RADIANS(%f))+SIN(RADIANS(%f))*SIN(RADIANS(latitud)))) AS distance FROM dashboard_quiickler WHERE (latitud BETWEEN %f AND %f) AND (longitud BETWEEN %f AND %f) AND estado = 1 HAVING distance < %i ORDER BY distance ASC" % (lat, lng, lat, box['min_lat'], box['max_lat'], box['min_lng'], box['max_lng'], distance)
        # quiickler = Quiickler.objects.raw(query)

        print len(list(quiickler))
        for q in quiickler:
            devices = GCMDevice.objects.filter(user=q.user)
            devices.send_message(
                None,
                extra={
                    "servicio_id": "%s" % instance.pk,
                    "mensaje": "Nuevo pedido",
                    "tipo": "servicio",
                    "estado": "%s" % instance.estado
                }
            )
            notificacion = NotificacionServicio.objects.create(
                quiickler=q,
                user=instance.user,
                servicio=instance
            )
            print notificacion

    if instance.estado == 'entregado':
        devices_user = GCMDevice.objects.filter(user=instance.user)
        devices_user.send_message(
            None,
            extra={
                "servicio_id": "%s" % instance.pk,
                "mensaje": "Llego tu pedido!.",
                "tipo": "servicio",
                "estado": "%s" % instance.estado
            }
        )
        for item in instance.pedido.productopedido_set.all():
            cantidad_stock = item.producto.cantidad
            cantidad_comprada = item.cantidad
            nueva_cantidad = cantidad_stock - cantidad_comprada
            item.producto.cantidad = nueva_cantidad
            item.producto.save()
        venta, created1 = Venta.objects.get_or_create(
            servicio=instance,
            quiickler=instance.quiickler,
            total=instance.total,
            gratis=True if instance.gratis else False
        )
        if created1:
            print "Se creo la venta"
        if not instance.gratis:
            pago, created2 = Pago.objects.get_or_create(
                servicio=instance,
                quiickler=instance.quiickler
            )
            if created2:
                print "Se creo el pago"
