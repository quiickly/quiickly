from django.db import models
from django.conf import settings
from accounts.models import TimeStampedModel
from dashboard.models import Pedido, Quiickler, Direccion


TIPO_SERVICIO = (
    ('express', 'Express'),
    ('programado', 'Programado'),
)
ESTADO_SERVICIO = (
    ('espera', 'En espera'),
    ('confirmando', 'Confirmando con quiickler'),
    ('entregado', 'Entregado'),
    ('noentregado', 'No entregado'),
    ('rechazado', 'Rechazado por los quiicklers'),
    ('sinrespuesta', 'Sin respuesta de los quiicklers')
)


class Servicio(TimeStampedModel):
    """ Modelo para los Servicios """

    pedido = models.OneToOneField(Pedido, related_name='servicios')
    quiickler = models.ForeignKey(Quiickler, null=True)
    fecha_confirmacion = models.DateTimeField(null=True, blank=True)
    fecha_entrega = models.DateTimeField(null=True, blank=True)
    fecha_programada = models.DateTimeField(null=True, blank=True)
    direccion = models.ForeignKey(Direccion, null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True)
    gratis = models.BooleanField(default=False)
    estado = models.CharField(
        "Estado",
        max_length=15,
        choices=ESTADO_SERVICIO,
        default='confirmando'
    )
    tipo = models.CharField(
        "Tipo de servicio",
        max_length=15,
        choices=TIPO_SERVICIO
    )
    longitud = models.FloatField('longitud')
    latitud = models.FloatField('latitud')
    total = models.FloatField(null=True)

    def __unicode__(self):
        return '%s: %s' % (self.tipo, self.estado)

OPCIONES_EVALUACION = (
    ('excelente', 'Excelente'),
    ('bueno', 'Bueno'),
    ('regular', 'Regular'),
    ('malo', 'Malo')
)


class Evaluacion(TimeStampedModel):
    """ Modelo para almacenar las evaluaciones del servicio """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='evaluaciones'
    )
    quiickler = models.ForeignKey(Quiickler)
    servicio = models.OneToOneField(Servicio)
    evaluacion = models.CharField(
        "Estado", max_length=15,
        choices=OPCIONES_EVALUACION
    )
    comentario = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.evaluacion


class Pago(TimeStampedModel):
    """ Modelo para almacenar los pagos """

    quiickler = models.ForeignKey(Quiickler, related_name='pagos')
    servicio = models.OneToOneField(Servicio)
    estado = models.BooleanField(default=False)

    def __unicode__(self):
        return 'Pagos de quiickler %s' % self.quiickler.nombre


class Venta(TimeStampedModel):
    """ Modelo para almacenar las ventas """

    quiickler = models.ForeignKey(Quiickler)
    servicio = models.OneToOneField(Servicio)
    total = models.FloatField(null=True)
    gratis = models.BooleanField(default=False)

    def __unicode__(self):
        return 'Pedido creado: %s' % self.servicio.created
