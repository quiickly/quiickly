from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import *
from dashboard.serializers import PedidoSerializer, DireccionSerializer
User = get_user_model()


class EvaluacionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Evaluacion
        fields = (
            'id', 'user', 'quiickler', 'servicio', 'evaluacion', 'comentario'
        )


class ServicioCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Servicio
        fields = (
            'id', 'user', 'longitud', 'latitud', 'direccion',
            'total', 'tipo', 'fecha_programada'
        )


class ServicioDetailSerializer(serializers.ModelSerializer):
    pedido = PedidoSerializer()
    direccion = DireccionSerializer()
    fecha_confirmacion = serializers.DateTimeField(
        format="%Y-%m-%d %H:%M:%S",
        input_formats=None
    )
    fecha_entrega = serializers.DateTimeField(
        format="%Y-%m-%d %H:%M:%S",
        input_formats=None
    )
    fecha_programada = serializers.DateTimeField(
        format="%Y-%m-%d %H:%M:%S",
        input_formats=None
    )

    class Meta:
        model = Servicio
        fields = (
            'id', 'user', 'longitud', 'latitud', 'quiickler',
            'direccion', 'total', 'tipo', 'pedido', 'estado',
            'fecha_confirmacion', 'fecha_entrega', 'fecha_programada'
        )
