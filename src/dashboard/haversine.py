from __future__ import division
import math


def getBoundaries(lat, lng, distance=1, earthRadius=6371):
    arrayList = {}
    cardinalCoords = {'north': 0, 'south': 180, 'east': 90, 'west': 270}
    rLat = math.radians(lat)
    rLng = math.radians(lng)
    rAngDist = distance/earthRadius
    for name, angle in cardinalCoords.items():
        rAngle = math.radians(angle)
        rLatB = math.asin(math.sin(rLat) * math.cos(rAngDist) + math.cos(rLat) * math.sin(rAngDist) * math.cos(rAngle))
        rLonB = rLng + math.atan2(math.sin(rAngle) * math.sin(rAngDist) * math.cos(rLat), math.cos(rAngDist) - math.sin(rLat) * math.sin(rLatB))
        arrayList[name] = {'lat': float(math.degrees(rLatB)), 'lng': float(math.degrees(rLonB))}

    return {'min_lat': arrayList['south']['lat'], 'max_lat': arrayList['north']['lat'], 'min_lng': arrayList['west']['lng'], 'max_lng': arrayList['east']['lng']}
