from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.core.urlresolvers import reverse_lazy, reverse
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.views.generic import ListView, View, TemplateView
from django.views.generic.edit import UpdateView, CreateView
from django.views.generic.detail import DetailView
from .models import *
from .forms import *
from servicios.models import Pago, Evaluacion, Venta, Servicio
import datetime
import dateutil
from django.db.models import Sum
import qsstats
from toexcel import queryset_to_workbook


User = get_user_model()


@login_required
def BorrarQuiickler(request, pk):
    """
    Detalle de un tipo de condon.
    """
    try:
        q = Quiickler.objects.get(pk=pk)
    except Quiickler.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        print q.user.is_active
        q.user.is_active = False
        q.estado = False
        q.user.save()
        q.save()
        return HttpResponseRedirect(reverse('dashboard:quiickler_list'))


class ProductoList(ListView):
    model = Producto
    template_name = 'dashboard/productos_list.html'

    #solo podra acceder si ha iniciado session
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise Http404("Esta pagina no existe!.")
        return super(ProductoList, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(ProductoList, self).get_context_data(**kwargs)
        # Add in the publisher
        disponibles = 0
        productos = Producto.objects.all()
        for p in productos:
            disponibles += p.cantidad

        context['disponibles'] = disponibles
        return context


class PagoCanceladoList(ListView):
    model = Pago
    template_name = 'dashboard/pagos_cancelados.html'

    #solo podra acceder si ha iniciado session
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise Http404("Esta pagina no existe!.")
        return super(PagoCanceladoList, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = Pago.objects.all().filter(estado=True)
        return queryset


class PagoPorRecibirList(ListView):
    model = Pago
    template_name = 'dashboard/pagos_recibir.html'

    #solo podra acceder si ha iniciado session
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise Http404("Esta pagina no existe!.")
        return super(PagoPorRecibirList, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = Pago.objects.all().filter(estado=False)
        return queryset


@login_required
def RegistrarPago(request, pk):
    """
    Registrar pago recibido.
    """
    try:
        p = Pago.objects.get(pk=pk)
    except Pago.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        if not p.estado:
            p.estado = True
            p.save()
        return HttpResponseRedirect(reverse('dashboard:pagos_sincancelar'))


class UserList(ListView):
    model = User
    template_name = 'dashboard/usuarios_list.html'

    #solo podra acceder si ha iniciado session
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise Http404("Esta pagina no existe!.")
        return super(UserList, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = User.objects.all().filter(quiickler=None)
        return queryset


class QuiicklerList(ListView):
    model = Quiickler

    #solo podra acceder si ha iniciado session
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise Http404("Esta pagina no existe!.")
        return super(QuiicklerList, self).dispatch(request, *args, **kwargs)


class QuiicklerListMapa(ListView):
    model = Quiickler
    template_name = 'dashboard/quiickler_mapa.html'

    #solo podra acceder si ha iniciado session
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise Http404("Esta pagina no existe!.")
        return super(QuiicklerListMapa, self).dispatch(request, *args, **kwargs)


class QuiicklerMapaBusqueda(ListView):
    model = RegistroBusqueda
    template_name = 'dashboard/quiickler_mapa_busqueda.html'

    #solo podra acceder si ha iniciado session
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise Http404("Esta pagina no existe!.")
        return super(QuiicklerMapaBusqueda, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        # Call the base implementation first to get a context
        context = super(QuiicklerMapaBusqueda, self).get_context_data(**kwargs)
        # Add in a QuerySet of all the books
        context['quiickler_list'] = Quiickler.objects.all()
        return context


class CreateQuiicklerView(View):

    #solo podra acceder si ha iniciado session
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise Http404("Esta pagina no existe!.")
        return super(CreateQuiicklerView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = UserForm()
        form_q = QuiicklerForm()
        return render(
            request, "dashboard/create_quiickler.html",
            {"user_form": form, "q_form": form_q}
        )

    def post(self, request, *args, **kwargs):
        form_user = UserForm(request.POST)
        form_q = QuiicklerForm(request.POST, request.FILES)

        if form_q.is_valid() and form_user.is_valid():
            user = form_user.save(commit=False)
            user.set_password(user.password)
            user.save()
            q = form_q.save(commit=False)
            q.user = user
            q.save()
            return HttpResponseRedirect(reverse('dashboard:quiickler_list'))

        else:
            print "no es valido"
            return render(
                request, "dashboard/create_quiickler.html",
                {"user_form": form_user, "q_form": form_q}
            )


class CreateProductoView(View):

    #solo podra acceder si ha iniciado session
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise Http404("Esta pagina no existe!.")
        return super(CreateProductoView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = ProductoForm()
        return render(
            request, "dashboard/create_producto.html",
            {"producto_form": form}
        )

    def post(self, request, *args, **kwargs):
        form = ProductoForm(request.POST, request.FILES)

        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('dashboard:productos_list'))

        else:
            return render(
                request, "dashboard/create_producto.html",
                {"producto_form": form}
            )


class ProductoUpdate(UpdateView):
    model = Producto
    form_class = ProductoForm
    template_name = 'dashboard/edit_producto.html'

    def get_success_url(self):
        messages.success(self.request, 'El producto fue actualizado.')
        return reverse('dashboard:producto_editar', kwargs={'pk': self.object.id})


class TipoCreate(CreateView):
    model = Tipo
    fields = ['tipo']
    success_url = reverse_lazy('dashboard:tipo_list')


class TipoList(ListView):
    model = Tipo
    template_name = 'dashboard/tipo_list.html'

    #solo podra acceder si ha iniciado session
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise Http404("Esta pagina no existe!.")
        return super(TipoList, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = Tipo.objects.all()
        return queryset


class TipoUpdate(UpdateView):
    model = Tipo
    fields = ['tipo']
    template_name = 'dashboard/edit_tipo.html'

    def get_success_url(self):
        messages.success(self.request, 'El tipo fue actualizado.')
        return reverse('dashboard:tipo_editar', kwargs={'pk': self.object.id})


class MarcaCreate(CreateView):
    model = Marca
    fields = ['nombre']
    success_url = reverse_lazy('dashboard:marca_list')


class MarcaUpdate(UpdateView):
    model = Marca
    fields = ['nombre']
    template_name = 'dashboard/edit_marca.html'

    def get_success_url(self):
        messages.success(self.request, 'La marca fue actualizada.')
        return reverse('dashboard:marca_editar', kwargs={'pk': self.object.id})


class MarcaList(ListView):
    model = Marca
    template_name = 'dashboard/marca_list.html'

    #solo podra acceder si ha iniciado session
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise Http404("Esta pagina no existe!.")
        return super(MarcaList, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = Marca.objects.all()
        return queryset


class EditQuiicklerView(View):

    #solo podra acceder si ha iniciado session
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise Http404("Esta pagina no existe!.")
        return super(EditQuiicklerView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        id = self.kwargs['quiickler_pk']
        q = get_object_or_404(Quiickler, pk=id)
        form = UserUpdateForm(instance=q.user)
        form_q = QuiicklerForm(instance=q)
        return render(
            request, "dashboard/edit_quiickler.html",
            {"user_form": form, "q_form": form_q}
        )

    def post(self, request, *args, **kwargs):
        id = self.kwargs['quiickler_pk']
        q = get_object_or_404(Quiickler, pk=id)
        form_user = UserUpdateForm(request.POST, instance=q.user)
        form_q = QuiicklerForm(request.POST, request.FILES, instance=q)

        if form_q.is_valid() and form_user.is_valid():
            form_user.save()
            form_q.save()
            messages.success(self.request, 'El quiickler fue actualizado.')
            return render(
                request, "dashboard/edit_quiickler.html",
                {"user_form": form_user, "q_form": form_q}
            )

        else:
            print "no valido"
            return render(
                request, "dashboard/edit_quiickler.html",
                {"user_form": form_user, "q_form": form_q}
            )


class DashboardView(View):

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise Http404("Esta pagina no existe!.")
        return super(DashboardView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        # ventas por mes, vieja implementacion, solo trae los meses en los que se ha realizado una venta.
        # truncate_month = connection.ops.date_trunc_sql('month','created')
        # venta_data = Venta.objects.extra({'month': truncate_month}).values('month').annotate(Count('pk'))

        # truncate_day = connection.ops.date_trunc_sql('day','created')
        # data = Venta.objects.extra({'day': truncate_day}).values('day').annotate(Count('pk'))
        ventas_dia = Venta.objects.filter(
            created__gt=datetime.date.today()
        ).count()
        usuarios_dia = User.objects.filter(
            date_joined__gt=datetime.date.today()
        ).count()
        servicios_rechazados = Servicio.objects.filter(
            estado='rechazado').count()

        # queryset y querysetstats para los datos del modelo Venta.
        qs = Venta.objects.all()
        qss = qsstats.QuerySetStats(qs, 'created')
        # Datos para grafico: Ventas diarias, ultimos 20 dias
        today = datetime.date.today()
        veinte_days_ago = today - datetime.timedelta(days=20)
        ventas_diarias = qss.time_series(veinte_days_ago, today)
        # datos para grafico: ventas mensuales
        doce_meses_ago = datetime.datetime.now() - dateutil.relativedelta.relativedelta(months=12)
        sales_month_data = qss.time_series(
            doce_meses_ago.date(),
            datetime.datetime.now().date(),
            interval='months'
        )
        sales_month = {
            "data": sales_month_data,
            "fecha_inicio": doce_meses_ago.date(),
            "fecha_fin": datetime.datetime.now().date()
        }

        # datos para grafico: Usuarios
        qs_user = User.objects.all()
        qss_user = qsstats.QuerySetStats(qs_user, 'date_joined')

        doce_meses_ago_user = datetime.datetime.now() - dateutil.relativedelta.relativedelta(months=12)
        users_month_data = qss_user.time_series(
            doce_meses_ago_user.date(),
            datetime.datetime.now().date(),
            interval='months'
        )
        sales_month = {
            "data": sales_month_data,
            "fecha_inicio": doce_meses_ago.date(),
            "fecha_fin": datetime.datetime.now().date()
        }

        # Total de ventas (acumulado)
        q_ventas = Venta.objects.filter(servicio__estado='entregado')
        qss_ventas = qsstats.QuerySetStats(q_ventas, 'created', Sum('total'))

        # Datos para los ultimos pedidos
        ultimos_servicios = Servicio.objects.all().order_by('-created')[:5]
        return render(
            request, "dashboard/dashboard.html",
            {
                'ventas_dia': ventas_dia,
                'usuarios_dia': usuarios_dia,
                'servicios_rechazados': servicios_rechazados,
                'data_sales_day': ventas_diarias,
                'data_sales_month': sales_month,
                'ultimos_servicios': ultimos_servicios,
                'total_ventas': qss_ventas.until_now(),
                'users_month_data': users_month_data

            }
        )


class ServicioList(ListView):
    model = Servicio
    template_name = 'dashboard/servicio_list.html'

    #solo podra acceder si ha iniciado session
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise Http404("Esta pagina no existe!.")
        return super(ServicioList, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = Servicio.objects.all().order_by('-created')
        return queryset


class ServicioDetailView(DetailView):

    model = Servicio
    template_name = 'dashboard/servicio_detail.html'

    def get_context_data(self, **kwargs):
        context = super(ServicioDetailView, self).get_context_data(**kwargs)
        # ejemplo de como agregar variables al contexto
        # context['now'] = timezone.now()
        return context


class VentaList(ListView):
    model = Pedido
    template_name = 'dashboard/ventas_list.html'

    #solo podra acceder si ha iniciado session
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_staff:
            raise Http404("Esta pagina no existe!.")
        return super(VentaList, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = Venta.objects.all()
        return queryset


class VentaDetailView(DetailView):

    model = Venta
    template_name = 'dashboard/venta_detail.html'

    def get_context_data(self, **kwargs):
        context = super(VentaDetailView, self).get_context_data(**kwargs)
        # ejemplo de como agregar variables al contexto
        # context['now'] = timezone.now()
        return context


def UserEmailtoExcel(request):
    queryset = User.objects.all().filter(quiickler=None)
    columns = (
        'email',
    )
    workbook = queryset_to_workbook(queryset, columns)
    response = HttpResponse(content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="UsersEmailQuiickly.xls"'
    workbook.save(response)
    return response
