from django.contrib import admin
from .models import *


@admin.register(Marca, Tipo, Producto, Quiickler, RegistroBusqueda)
class DashAdmin(admin.ModelAdmin):
    pass


@admin.register(Direccion)
class DireccionDashAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'direccion',
        'nombre',
        'barrio'
    )
    list_filter = (
        'user',
        'direccion',
        'nombre',
        'barrio'
    )
    search_fields = [
        'user__email',
        'direccion',
        'nombre',
        'barrio'
    ]


class ProductoInline(admin.TabularInline):
    model = ProductoPedido
    extra = 2  # how many rows to show


@admin.register(Pedido)
class PedidoAdminInline(admin.ModelAdmin):
    inlines = (ProductoInline,)
