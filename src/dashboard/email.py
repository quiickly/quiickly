# -*- coding: utf-8 -*-
import requests
from django.conf import settings
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import EmailMultiAlternatives


def send_simple_message(email, subject, text):
    return requests.post(
        "https://api.mailgun.net/v3/mg.script.com.co/messages",
        auth=("api", "key-1d5af07f47f5859f7037325b56eb77d1"),
        data={"from": "Quiickly <quiicklyapp@gmail.com>",
              "to": [email],
              "subject": subject,
              "text": text,
              "html": "<html>HTML version of the body</html>"})


def EmailRegistrar(email, nombre):
    subject = 'Bienvenido a Quiickly'
    from_email = settings.CORREO_ENVIO
    to = email
    html_content = render_to_string(
        'dashboard/registrar_action.html',
        {'nombre': nombre}
    )
    text_content = strip_tags(html_content)
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
    msg.attach_alternative(html_content, "text/html")
    msg.send()


def EmailRecuperarPassword(email, nombre, password):
    subject = 'Recuperación de contraseña'
    from_email = settings.CORREO_ENVIO
    to = email
    html_content = render_to_string(
        'dashboard/recuperar_password_email.html',
        {'nombre': nombre, 'password': password}
    )
    text_content = strip_tags(html_content)

    return requests.post(
        "https://api.mailgun.net/v3/mg.script.com.co/messages",
        auth=("api", "key-1d5af07f47f5859f7037325b56eb77d1"),
        data={"from": "Quiickly <quiicklyapp@gmail.com>",
              "to": [to],
              "subject": subject,
              "text": text_content,
              "html": html_content})
