# -*- coding: utf-8 -*-
from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import *
User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    imagen = serializers.SlugField(source='profile.picture')

    class Meta:
        model = User
        fields = ('id', 'name', 'password', 'email', 'imagen')
        write_only_fields = ('password',)
        read_only_fields = ('id', 'imagen')

    def create(self, validated_data):
        user = User.objects.create(
            name=validated_data['name'],
            email=validated_data['email'],
        )

        user.set_password(validated_data['password'])
        user.save()

        return user


class UserUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'name', 'password', 'email')
        read_only_fields = ('id',)

    def update(self, instance, validated_data):
        instance.email = validated_data.get('email', instance.email)
        instance.password = validated_data.get('password', instance.content)
        instance.save()
        return instance


class DireccionUserSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Direccion
        fields = (
            'id', 'user', 'nombre', 'barrio', 'direccion'
        )
        read_only_fields = ('user',)


class DireccionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Direccion
        fields = (
            'id', 'nombre', 'direccion', 'user', 'barrio',
        )
        read_only_fields = ('user',)
        read_only_fields = ('id',)


class ProductoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Producto
        fields = (
            'id', 'nombre', 'marca', 'tipo',
            'cantidad', 'estado', 'precio', 'imagen'
        )


class ProductoOndemandSerializer(serializers.ModelSerializer):

    class Meta:
        model = Producto
        fields = (
            'id', 'nombre', 'marca', 'tipo',
            'precio', 'imagen_ondemand'
        )


class QuiicklerSerializer(serializers.ModelSerializer):

    class Meta:
        model = Quiickler
        fields = (
            'id', 'nombre', 'placa', 'estado', 'numero',
            'longitud', 'latitud', 'direccion', 'ciudad', 'foto'
        )
        read_only_fields = ('id', 'foto')


class ProductoPedidoSerializer(serializers.HyperlinkedModelSerializer):

    producto = serializers.IntegerField()

    class Meta:
        model = ProductoPedido
        fields = ('producto', 'cantidad')


class ProductoPedidoSerializer2(serializers.ModelSerializer):
    producto = serializers.IntegerField(source='producto.pk')

    class Meta:
        model = ProductoPedido
        fields = ('producto', 'cantidad')


class PedidoSerializer(serializers.ModelSerializer):
    productos = ProductoPedidoSerializer2(
        source='productopedido_set',
        many=True
    )

    class Meta:
        model = Pedido
        fields = (
            'id', 'user', 'productos', 'total', 'created'
        )


class PedidoCreateSerializer(serializers.ModelSerializer):
    productos = ProductoPedidoSerializer(source='productopedido_set', many=True)

    class Meta:
        model = Pedido
        fields = (
            'id', 'user', 'productos', 'total'
        )

    def create(self, validated_data):
        print validated_data
        productos_data = validated_data.pop('productopedido_set')
        print productos_data
        pedido = Pedido.objects.create(**validated_data)
        pedido.save()
        for productos in productos_data:
            producto_object = Producto.objects.get(pk=productos['producto'])
            producto_pedido = ProductoPedido.objects.create(
                producto=producto_object,
                pedido=pedido,
                cantidad=int(productos['cantidad'])
            )
        return pedido


class TipoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tipo
        fields = (
            'id', 'tipo'
        )


class MarcaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Marca
        fields = (
            'id', 'nombre'
        )


class DireccionUserCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Direccion
        fields = (
            'id', 'nombre', 'direccion', 'barrio'
        )
        read_only_fields = ('id',)


class UserCreateSerializer(serializers.ModelSerializer):
    direcciones = DireccionUserCreateSerializer(many=True)

    class Meta:
        model = User
        fields = ('id', 'name', 'password', 'email', 'direcciones')
        write_only_fields = ('password',)
        read_only_fields = ('id',)

    def create(self, validated_data):
        user = User.objects.create(
            name=validated_data['name'],
            email=validated_data['email'],
        )

        user.set_password(validated_data['password'])
        user.save()
        print validated_data
        direcciones_data = validated_data.pop('direcciones')
        print direcciones_data
        for direccion in direcciones_data:
            Direccion.objects.create(user=user, **direccion)
        return user
