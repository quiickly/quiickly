from dashboard.haversine import getBoundaries
from dashboard.models import Quiickler


def BusquedaQuiicklers(lat, lng, distance):
    box = getBoundaries(lat, lng, distance)
    query = "select *,(6371 * ACOS(COS( RADIANS(%f))*COS(RADIANS(latitud))*COS(RADIANS(longitud)-RADIANS(%f))+SIN(RADIANS(%f))*SIN(RADIANS(latitud)))) AS distance FROM dashboard_quiickler WHERE (latitud BETWEEN %f AND %f) AND (longitud BETWEEN %f AND %f) AND estado = 1 HAVING distance < %i ORDER BY distance ASC" % (lat, lng, lat, box['min_lat'], box['max_lat'], box['min_lng'], box['max_lng'], distance)
    quiicklers = Quiickler.objects.raw(query)
    return quiicklers
