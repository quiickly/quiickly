from __future__ import unicode_literals
from django import forms
from django.contrib.auth import get_user_model
from .models import *

User = get_user_model()


class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['email', 'name', 'password']
        widgets = {
            'password': forms.PasswordInput(),
        }


class UserUpdateForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['email', 'name']


class ProductoForm(forms.ModelForm):

    class Meta:
        model = Producto
        exclude = ['created', 'modified']


class QuiicklerForm(forms.ModelForm):

    class Meta:
        model = Quiickler
        exclude = ['user', 'created', 'modified', 'longitud', 'latitud']


class TipoForm(forms.ModelForm):

    class Meta:
        model = Tipo
        fields = ['tipo', ]


class MarcaForm(forms.ModelForm):

    class Meta:
        model = Marca
        fields = ['nombre', ]
