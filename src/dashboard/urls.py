from django.conf.urls import url
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView
from django.contrib.admin.views.decorators import staff_member_required


from .views import *

urlpatterns = [

    url(r'^info/$', staff_member_required(DashboardView.as_view()), name="dash"),
    url(r'^$', RedirectView.as_view(url='info/')),
    url(r'^usuarios/$', UserList.as_view(), name="usuarios_list"),
    url(r'^usuarios/toexcel/$', 'dashboard.views.UserEmailtoExcel', name="usuarios_email_to_excel"),
    #url(r'^estadisticas/$', EstadisticaView.as_view(), name="estadistica"),
    url(r'^productos/$', ProductoList.as_view(), name="productos_list"),
    url(r'^productos/crear/$', CreateProductoView.as_view(), name="productos_crear"),
    url(r'^productos/editar/(?P<pk>[0-9]+)/$', ProductoUpdate.as_view(), name='producto_editar'),
    url(r'^productos/tipos/$', TipoList.as_view(), name='tipo_list'),
    url(r'^productos/tipos/crear/$', TipoCreate.as_view(), name='tipo_crear'),
    url(r'^productos/tipos/editar/(?P<pk>[0-9]+)/$', TipoUpdate.as_view(), name='tipo_editar'),
    url(r'^productos/marcas/$', MarcaList.as_view(), name='marca_list'),
    url(r'^productos/marcas/crear/$', MarcaCreate.as_view(), name='marca_crear'),
    url(r'^productos/marcas/editar/(?P<pk>[0-9]+)/$', MarcaUpdate.as_view(), name='marca_editar'),
    url(r'^servicios/$', ServicioList.as_view(), name='servicios_list'),
    url(r'^servicios/(?P<pk>[0-9]+)/$', ServicioDetailView.as_view(), name='servicio-detail'),
    url(r'^pagos/$', RedirectView.as_view(url='sincancelar/')),
    url(r'^pagos/registrar/(?P<pk>[0-9]+)/$', 'dashboard.views.RegistrarPago', name='pagos_registrar'),
    url(r'^pagos/cancelados/$', PagoCanceladoList.as_view(), name="pagos_cancelados"),
    url(r'^pagos/sincancelar/$', PagoPorRecibirList.as_view(), name="pagos_sincancelar"),
    url(r'^ventas/$', VentaList.as_view(), name="ventas_list"),
    url(r'^ventas/(?P<pk>[0-9]+)/$', VentaDetailView.as_view(), name='venta-detail'),
    url(r'^quiicklers/$', QuiicklerList.as_view(), name="quiickler_list"),
    url(r'^quiicklers/mapaBusqueda/$', QuiicklerMapaBusqueda.as_view(), name="quiickler_mapa_busqueda"),
    url(r'^quiicklers/mapa/$', QuiicklerListMapa.as_view(), name="quiickler_mapa"),
    url(r'^quiicklers/crear/$', CreateQuiicklerView.as_view(), name="quiickler_create"),
    url(r'^quiicklers/edit/(?P<quiickler_pk>\d+)/$', EditQuiicklerView.as_view(), name='quiickler_edit'),
    url(r'^quiicklers/eliminar/(?P<pk>\d+)/$', 'dashboard.views.BorrarQuiickler', name='quiickler_eliminar'),

]
