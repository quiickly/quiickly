# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings
from django.db.models.signals import pre_save, pre_delete, post_save, post_delete
from django.dispatch import receiver
from dashboard.haversine import getBoundaries
from push_notifications.models import GCMDevice
from accounts.models import TimeStampedModel


ZONA = (
    ('sur', 'Sur'),
    ('norte', 'Norte'),
    ('occidente', 'Occidente'),
    ('oriente', 'Oriente')
)


class Direccion(TimeStampedModel):
    """ Modelo para los lavaderos"""

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='direcciones')

    direccion = models.CharField(max_length=70)
    nombre = models.CharField("Nombre de la direccion", max_length=50)
    barrio = models.CharField("Barrio", max_length=50)

    def __unicode__(self):
        return '%s' % self.nombre


class Marca(TimeStampedModel):
    """ Modelo para manejar las marcas de los condones"""

    nombre = models.CharField("Marca", max_length=50)

    def __unicode__(self):
        return '%s' % self.nombre


class Tipo(TimeStampedModel):
    """ Modelo para manejar las marcas de los condones"""

    tipo = models.CharField("Tipo", max_length=50)

    def __unicode__(self):
        return '%s' % self.tipo


ESTADO = (
    ('disponible', 'Disponible'),
    ('agotado', 'Agotado'),
)


DISTRIBUCION = (
    ('app', 'Aplicacion'),
    ('web', 'Web'),
    ('webapp', 'Web y App')
)


class Producto(TimeStampedModel):
    """ Modelo para los productos """

    nombre = models.CharField("Nombre", max_length=50)
    descripcion = models.TextField("Descripcion", null=True, blank=True)
    marca = models.ForeignKey(Marca, related_name='marcas')
    tipo = models.ForeignKey(Tipo, related_name='tipos')
    cantidad = models.PositiveIntegerField()
    estado = models.CharField("Estado", max_length=15, choices=ESTADO)
    distribucion = models.CharField(
        "Canal de distribucion",
        max_length=15,
        choices=DISTRIBUCION,
        default='app'
    )
    precio = models.FloatField("Precio express")
    precio_programado = models.FloatField(null=True)
    imagen = models.ImageField(upload_to='img_producto/')
    imagen_ondemand = models.ImageField(
        upload_to='img_producto/',
        null=True,
        blank=True
    )

    def __unicode__(self):
        return '%s' % self.nombre

    def save(self, *args, **kwargs):
        if self.cantidad <= 0 and self.estado == 'disponible':
            self.estado = 'agotado'
            super(Producto, self).save(*args, **kwargs)
        else:
            super(Producto, self).save(*args, **kwargs)

    def total(self):
        return self.precio * self.cantidad


class Quiickler(TimeStampedModel):
    """ Modelo para los quiicklers """

    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    nombre = models.CharField("Nombre", max_length=70)
    placa = models.CharField("Placa", max_length=10, null=True, blank=True)
    estado = models.BooleanField(default=False)
    longitud = models.FloatField('longitud', default='-75.2653764')
    latitud = models.FloatField('latitud', default='10.5625653')
    direccion = models.CharField(max_length=70)
    ciudad = models.CharField("Ciudad", max_length=50, null=True, blank=True)
    numero = models.BigIntegerField(null=True)
    foto = models.ImageField(null=True, blank=True, upload_to='quiickler_pics/')

    def __unicode__(self):
        return '%s' % self.nombre


class Pedido(TimeStampedModel):
    """ Modelo para los productos """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='pedidos'
    )
    producto = models.ManyToManyField(
        Producto, through='ProductoPedido',
        through_fields=('pedido', 'producto')
    )
    total = models.FloatField()

    def __unicode__(self):
        return '%s : %s' % (self.user.email, self.created)


class ProductoPedido(TimeStampedModel):
    """ Modelo para los productos que estan en un pedido """

    producto = models.ForeignKey(Producto)
    pedido = models.ForeignKey(Pedido)
    cantidad = models.IntegerField()

    def __unicode__(self):
        return '%s' % self.producto.nombre

    def subtotal(self):
        print self.cantidad, " $ ", self.producto.precio
        return self.cantidad * self.producto.precio


class RegistroBusqueda(TimeStampedModel):
    """docstring for RegistroBusqueda"""
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    longitud = models.FloatField('longitud')
    latitud = models.FloatField('latitud')

    def __unicode__(self):
        return 'Busqueda: %s' % self.user.email


@receiver(post_save, sender=Producto)
def model_post_save(sender, instance, **kwargs):
    cantidad_producto = instance.cantidad
    if cantidad_producto == 0:
        instance.estado = 'agotado'


@receiver(post_save, sender=Pedido)
def pedido_post_save(sender, instance, created, **kwargs):
    print "Creado: ", created
    print "Instance: ", instance
    print "ProductoPedido: ", instance.productopedido_set.all()
