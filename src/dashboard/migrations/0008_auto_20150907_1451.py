# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0007_auto_20150902_1858'),
    ]

    operations = [
        migrations.AlterField(
            model_name='quiickler',
            name='numero',
            field=models.BigIntegerField(null=True),
        ),
    ]
