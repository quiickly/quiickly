# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0009_auto_20150909_1102'),
    ]

    operations = [
        migrations.CreateModel(
            name='Venta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterField(
            model_name='pedido',
            name='estado',
            field=models.CharField(max_length=15, verbose_name=b'Estado', choices=[(b'espera', b'En espera'), (b'entregado', b'Entregado'), (b'noentregado', b'No entregado'), (b'rechazado', b'Rechazado por los quiicklers')]),
        ),
        migrations.AddField(
            model_name='venta',
            name='pedido',
            field=models.ForeignKey(to='dashboard.Pedido'),
        ),
        migrations.AddField(
            model_name='venta',
            name='quiickler',
            field=models.ForeignKey(to='dashboard.Quiickler'),
        ),
    ]
