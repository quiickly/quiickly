# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0018_auto_20151023_1133'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='evaluacion',
            name='pedido',
        ),
        migrations.RemoveField(
            model_name='evaluacion',
            name='quiickler',
        ),
        migrations.RemoveField(
            model_name='evaluacion',
            name='user',
        ),
        migrations.RemoveField(
            model_name='pago',
            name='pedido',
        ),
        migrations.RemoveField(
            model_name='pago',
            name='quiickler',
        ),
        migrations.RemoveField(
            model_name='venta',
            name='pedido',
        ),
        migrations.RemoveField(
            model_name='venta',
            name='quiickler',
        ),
        migrations.DeleteModel(
            name='Evaluacion',
        ),
        migrations.DeleteModel(
            name='Pago',
        ),
        migrations.DeleteModel(
            name='Venta',
        ),
    ]
