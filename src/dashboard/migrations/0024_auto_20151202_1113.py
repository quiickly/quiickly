# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0023_registrobusqueda'),
    ]

    operations = [
        migrations.AddField(
            model_name='producto',
            name='distribucion',
            field=models.CharField(default=b'app', max_length=15, verbose_name=b'Canal de distribucion', choices=[(b'app', b'Aplicacion'), (b'web', b'Web'), (b'webapp', b'Web y App')]),
        ),
        migrations.AddField(
            model_name='producto',
            name='imagen_ondemand',
            field=models.ImageField(null=True, upload_to=b'img_producto/'),
        ),
    ]
