# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('dashboard', '0006_auto_20150902_1755'),
    ]

    operations = [
        migrations.CreateModel(
            name='Evaluacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('evaluacion', models.CharField(max_length=15, verbose_name=b'Estado', choices=[(b'excelente', b'Excelente'), (b'bueno', b'Bueno'), (b'regular', b'Regular'), (b'malo', b'Malo')])),
                ('pedido', models.ForeignKey(to='dashboard.Pedido')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='quiickler',
            name='numero',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='evaluacion',
            name='quiickler',
            field=models.ForeignKey(to='dashboard.Quiickler'),
        ),
        migrations.AddField(
            model_name='evaluacion',
            name='user',
            field=models.ForeignKey(related_name='evaluaciones', to=settings.AUTH_USER_MODEL),
        ),
    ]
