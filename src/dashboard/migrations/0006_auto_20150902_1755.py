# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0005_auto_20150901_1840'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pedido',
            name='estado',
            field=models.CharField(max_length=15, verbose_name=b'Estado', choices=[(b'espera', b'En espera'), (b'entregado', b'Entregado'), (b'noentregado', b'No entregado')]),
        ),
        migrations.AlterField(
            model_name='quiickler',
            name='placa',
            field=models.CharField(max_length=10, null=True, verbose_name=b'Placa', blank=True),
        ),
    ]
