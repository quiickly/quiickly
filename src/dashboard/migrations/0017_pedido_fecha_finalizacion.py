# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0016_pedido_fecha_confirmacion'),
    ]

    operations = [
        migrations.AddField(
            model_name='pedido',
            name='fecha_finalizacion',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
