# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0021_producto_precio_programado'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producto',
            name='precio',
            field=models.FloatField(verbose_name=b'Precio express'),
        ),
        migrations.AlterField(
            model_name='quiickler',
            name='ciudad',
            field=models.CharField(max_length=50, null=True, verbose_name=b'Ciudad', blank=True),
        ),
    ]
