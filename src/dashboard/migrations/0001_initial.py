# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Direccion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('direccion', models.CharField(max_length=70)),
                ('nombre', models.CharField(max_length=50, verbose_name=b'Nombre de la direccion')),
                ('barrio', models.CharField(max_length=50, verbose_name=b'Barrio')),
                ('zona', models.CharField(max_length=12, verbose_name=b'Zona', choices=[(b'sur', b'Sur'), (b'norte', b'Norte'), (b'occidente', b'Occidente'), (b'oriente', b'Oriente')])),
                ('user', models.ForeignKey(related_name='direcciones', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
