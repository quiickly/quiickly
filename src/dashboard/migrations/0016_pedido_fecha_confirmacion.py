# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0015_auto_20150930_1625'),
    ]

    operations = [
        migrations.AddField(
            model_name='pedido',
            name='fecha_confirmacion',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
