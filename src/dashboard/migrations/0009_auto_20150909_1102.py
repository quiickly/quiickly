# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0008_auto_20150907_1451'),
    ]

    operations = [
        migrations.AddField(
            model_name='producto',
            name='descripcion',
            field=models.TextField(null=True, verbose_name=b'Descripcion', blank=True),
        ),
        migrations.AlterField(
            model_name='quiickler',
            name='latitud',
            field=models.FloatField(default=b'10.5625653', verbose_name=b'latitud'),
        ),
        migrations.AlterField(
            model_name='quiickler',
            name='longitud',
            field=models.FloatField(default=b'-75.2653764', verbose_name=b'longitud'),
        ),
    ]
