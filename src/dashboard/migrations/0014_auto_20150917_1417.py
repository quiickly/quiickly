# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0013_venta_total'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pedido',
            name='estado',
            field=models.CharField(default=b'espera', max_length=15, verbose_name=b'Estado', choices=[(b'espera', b'En espera'), (b'entregado', b'Entregado'), (b'noentregado', b'No entregado'), (b'rechazado', b'Rechazado por los quiicklers')]),
        ),
        migrations.AlterField(
            model_name='producto',
            name='cantidad',
            field=models.PositiveIntegerField(),
        ),
    ]
