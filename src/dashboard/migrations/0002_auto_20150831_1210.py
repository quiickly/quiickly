# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('dashboard', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Marca',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('nombre', models.CharField(max_length=50, verbose_name=b'Marca')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Pago',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('estado', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Pedido',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('total', models.FloatField()),
                ('estado', models.CharField(max_length=15, verbose_name=b'Estado', choices=[(b'espera', b'En espera'), (b'entregado', b'Entregado')])),
                ('longitud', models.FloatField(verbose_name=b'longitud')),
                ('latitud', models.FloatField(verbose_name=b'latitud')),
                ('direccion', models.ForeignKey(to='dashboard.Direccion')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Producto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('nombre', models.CharField(max_length=50, verbose_name=b'Nombre')),
                ('cantidad', models.IntegerField()),
                ('estado', models.CharField(max_length=15, verbose_name=b'Estado', choices=[(b'disponible', b'Disponible'), (b'agotado', b'Agotado')])),
                ('precio', models.IntegerField()),
                ('imagen', models.ImageField(upload_to=b'img_producto/')),
                ('marca', models.ForeignKey(related_name='marcas', to='dashboard.Marca')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ProductoPedido',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('cantidad', models.IntegerField()),
                ('pedido', models.ForeignKey(to='dashboard.Pedido')),
                ('producto', models.ForeignKey(to='dashboard.Producto')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Quiickler',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('nombre', models.CharField(max_length=70, verbose_name=b'Nombre')),
                ('placa', models.CharField(max_length=10, verbose_name=b'Placa')),
                ('estado', models.BooleanField(default=False)),
                ('longitud', models.FloatField(verbose_name=b'longitud')),
                ('latitud', models.FloatField(verbose_name=b'latitud')),
                ('direccion', models.CharField(max_length=70)),
                ('barrio', models.CharField(max_length=50, verbose_name=b'Barrio')),
                ('zona', models.CharField(max_length=12, verbose_name=b'Zona', choices=[(b'sur', b'Sur'), (b'norte', b'Norte'), (b'occidente', b'Occidente'), (b'oriente', b'Oriente')])),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Tipo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('tipo', models.CharField(max_length=50, verbose_name=b'Tipo')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='producto',
            name='tipo',
            field=models.ForeignKey(related_name='tipos', to='dashboard.Tipo'),
        ),
        migrations.AddField(
            model_name='pedido',
            name='producto',
            field=models.ManyToManyField(to='dashboard.Producto', through='dashboard.ProductoPedido'),
        ),
        migrations.AddField(
            model_name='pedido',
            name='user',
            field=models.ForeignKey(related_name='pedidos', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='pago',
            name='pedido',
            field=models.ForeignKey(related_name='servicios', to='dashboard.Pedido'),
        ),
        migrations.AddField(
            model_name='pago',
            name='quiickler',
            field=models.ForeignKey(related_name='pagos', to='dashboard.Quiickler'),
        ),
    ]
