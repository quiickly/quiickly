# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0010_auto_20150914_0941'),
    ]

    operations = [
        migrations.AlterField(
            model_name='venta',
            name='pedido',
            field=models.OneToOneField(to='dashboard.Pedido'),
        ),
    ]
