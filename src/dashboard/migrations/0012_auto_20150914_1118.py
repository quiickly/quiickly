# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0011_auto_20150914_1016'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pago',
            name='pedido',
            field=models.OneToOneField(related_name='servicio', to='dashboard.Pedido'),
        ),
    ]
