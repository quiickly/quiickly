# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0004_auto_20150901_0900'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pedido',
            name='quiickler',
            field=models.ForeignKey(blank=True, to='dashboard.Quiickler', null=True),
        ),
    ]
