# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0002_auto_20150831_1210'),
    ]

    operations = [
        migrations.AddField(
            model_name='pedido',
            name='quiickler',
            field=models.ForeignKey(to='dashboard.Quiickler', null=True),
        ),
    ]
