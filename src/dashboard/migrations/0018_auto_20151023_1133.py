# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0017_pedido_fecha_finalizacion'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='pedido',
            name='direccion',
        ),
        migrations.RemoveField(
            model_name='pedido',
            name='estado',
        ),
        migrations.RemoveField(
            model_name='pedido',
            name='fecha_confirmacion',
        ),
        migrations.RemoveField(
            model_name='pedido',
            name='fecha_finalizacion',
        ),
        migrations.RemoveField(
            model_name='pedido',
            name='latitud',
        ),
        migrations.RemoveField(
            model_name='pedido',
            name='longitud',
        ),
        migrations.RemoveField(
            model_name='pedido',
            name='quiickler',
        ),
        migrations.RemoveField(
            model_name='quiickler',
            name='barrio',
        ),
        migrations.RemoveField(
            model_name='quiickler',
            name='zona',
        ),
        migrations.AddField(
            model_name='quiickler',
            name='ciudad',
            field=models.CharField(max_length=50, null=True, verbose_name=b'Ciudad'),
        ),
        migrations.AddField(
            model_name='quiickler',
            name='foto',
            field=models.ImageField(null=True, upload_to=b'quiickler_pics/', blank=True),
        ),
    ]
