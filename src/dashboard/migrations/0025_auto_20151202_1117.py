# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0024_auto_20151202_1113'),
    ]

    operations = [
        migrations.AlterField(
            model_name='producto',
            name='imagen_ondemand',
            field=models.ImageField(null=True, upload_to=b'img_producto/', blank=True),
        ),
    ]
