# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0020_remove_direccion_zona'),
    ]

    operations = [
        migrations.AddField(
            model_name='producto',
            name='precio_programado',
            field=models.FloatField(null=True),
        ),
    ]
