# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0019_auto_20151023_1210'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='direccion',
            name='zona',
        ),
    ]
