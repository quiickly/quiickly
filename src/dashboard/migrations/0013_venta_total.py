# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dashboard', '0012_auto_20150914_1118'),
    ]

    operations = [
        migrations.AddField(
            model_name='venta',
            name='total',
            field=models.FloatField(null=True),
        ),
    ]
