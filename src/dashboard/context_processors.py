from django.core.urlresolvers import reverse

def menu(request):
    menu = {'menu': [
        {'name': 'Dashboard', 'icono': 'fa-dashboard' , 'url': '/dashboard/info', 'link': reverse('dashboard:dash')
        },
        {'name': 'Quiicklers', 'icono': 'fa fa-motorcycle' , 'url': '/dashboard/quiicklers','submenu':[
                { 'sname':'Listar', 'surl':reverse('dashboard:quiickler_list')},
                { 'sname':'Crear', 'surl':reverse('dashboard:quiickler_create')},
                { 'sname':'Mapa', 'surl':reverse('dashboard:quiickler_mapa')},
                { 'sname':'Mapa de busqueda', 'surl':reverse('dashboard:quiickler_mapa_busqueda')},
                ]
        },
        {'name': 'Productos', 'icono': 'fa-archive' , 'url': '/dashboard/productos','submenu':[
                { 'sname':'Listar Productos', 'surl':reverse('dashboard:productos_list')},
                { 'sname':'Crear Productos', 'surl':reverse('dashboard:productos_crear')},
                { 'sname':'Listar Tipo', 'surl':reverse('dashboard:tipo_list')},
                { 'sname':'Crear Tipo', 'surl':reverse('dashboard:tipo_crear')},
                { 'sname':'Listar Marcas', 'surl':reverse('dashboard:marca_list')},
                { 'sname':'Crear Marca', 'surl':reverse('dashboard:marca_crear')},
                ]
        },
        {'name': 'Usuarios', 'icono': 'fa-users' , 'url': '/dashboard/usuarios','submenu':[
                { 'sname':'Listar', 'surl':reverse('dashboard:usuarios_list')},
                ]
        },
        {'name': 'Servicios', 'icono': 'fa-shopping-cart' , 'url': '/dashboard/servicios','submenu':[
                { 'sname':'Listar', 'surl':reverse('dashboard:servicios_list')},
                ]
        },
        {'name': 'Ventas', 'icono': 'fa-cart-arrow-down' , 'url': '/dashboard/ventas','submenu':[
                { 'sname':'Listar', 'surl':reverse('dashboard:ventas_list')},
                ]
        },
        {'name': 'Pagos', 'icono': 'fa-money' , 'url': '/dashboard/pagos','submenu':[
                { 'sname':'Cancelados', 'surl':reverse('dashboard:pagos_cancelados')},
                { 'sname':'Por recibir', 'surl':reverse('dashboard:pagos_sincancelar')},
                ]
        },
        {'name': 'Estadisticas', 'icono': 'fa-bar-chart' , 'url': '/dashboard/estadisticas', 'link':'#'
        },

    ]}
    for men in menu['menu']:
        if men['url'] in request.path:
            men['active'] = True
    return menu
