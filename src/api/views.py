# -*- coding: utf-8 -*-
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.hashers import make_password
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import permissions, status, parsers, renderers
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.views import APIView
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.authtoken.models import Token
from dashboard.serializers import *
from dashboard.models import *
from servicios.serializers import *
from servicios.models import Servicio, Pago
from config.models import Configuration
from notificacion.models import NotificacionServicio
from dashboard.haversine import getBoundaries
from dashboard.email import EmailRecuperarPassword
from profiles.models import Profile
from push_notifications.models import GCMDevice
from push_notifications.api.rest_framework import GCMDeviceSerializer
import hashlib
import random
import re
import datetime


class ObtainAuthToken(APIView):
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (
        parsers.FormParser,
        parsers.MultiPartParser,
        parsers.JSONParser
    )
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = AuthTokenSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)

        return Response({'token': token.key})


class ObtainAuthTokenQuiickler(APIView):
    throttle_classes = ()
    permission_classes = ()
    parser_classes = (
        parsers.FormParser,
        parsers.MultiPartParser,
        parsers.JSONParser
    )
    renderer_classes = (renderers.JSONRenderer,)
    serializer_class = AuthTokenSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        try:
            quiickler = Quiickler.objects.get(user=user)
            token, created = Token.objects.get_or_create(user=user)
            return Response({'token': token.key})
        except Quiickler.DoesNotExist:
            return Response({'error': "No es un Quiickler!."})


# registrar device android para las notificaciones push

@api_view(['POST'])
@permission_classes((permissions.IsAuthenticated, ))
def registrarAndroid(request):
    if request.method == 'POST':
        serializer = GCMDeviceSerializer(data=request.data)
        if serializer.is_valid():
            try:
                android_device = GCMDevice.objects.get(
                    registration_id=request.data['registration_id'],
                    user=request.user
                )
                serializer2 = GCMDeviceSerializer(android_device)
                return Response(
                    serializer2.data,
                    status=status.HTTP_201_CREATED
                )
            except ObjectDoesNotExist:
                serializer.save(user=request.user)
                return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            print serializer.errors
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


@api_view(['POST'])
@permission_classes((permissions.AllowAny, ))
def createUser(request):
    if request.method == 'POST':
        serializer = UserCreateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(
                serializer.errors,
                status=status.HTTP_400_BAD_REQUEST
            )


@api_view(['POST'])
@permission_classes((permissions.AllowAny, ))
def olvidePassword(request):
    if request.method == 'POST':
        try:
            user = User.objects.get(email=request.data['email'])
        except ObjectDoesNotExist:
            return Response(
                {'error': 'El correo no se encuentra en la base de datos!.'},
                status=status.HTTP_400_BAD_REQUEST
            )
        salt = hashlib.sha1(str(random.random())).hexdigest()[:4]
        clave = hashlib.sha1(salt).hexdigest()[:6]
        nombre = user.name if user.name else user.email
        email = EmailRecuperarPassword(user.email, nombre, clave)
        if email.status_code == 200:
            user.password = make_password(clave)
            user.save()
            return Response(
                {'mensaje': 'se envio correctamente su nueva clave a su correo.'}
            )
        else:
            return Response(
                {'mensaje': 'No se pudo enviar correctamente el email, la clave no sufrio ningun cambio.'}
            )
        return Response(email)


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated, ))
def detalleUser(request):
    """
    Detalle del del usuario, y eliminar,
    es necesario estar autenticado.
    """

    try:
        user = User.objects.get(pk=request.user.pk)
    except User.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = UserSerializer(user)
        return Response(serializer.data)


@api_view(['POST'])
@permission_classes((permissions.IsAuthenticated, ))
def actualizarDatos(request):
    if request.method == 'POST':
        if 'email' in request.data and 'name' in request.data:
            if len(request.data['email']) == 0 or len(request.data['name']) == 0:
                return Response({
                    'error': 'Los datos proporcionados no son correctos o estan vacios!.'
                })
            else:
                if re.match('^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,15}$',request.data['email'].lower()):
                    user = request.user
                    if len(request.data['email']) == 0:
                        return Response({
                            'error': 'Los datos proporcionados no son correctos o estan vacios!.'},
                            status=status.HTTP_400_BAD_REQUEST
                        )
                    if user.email == request.data['email']:
                        # si el email que llega en los parametros es el
                        # mismo del usuario, no hay necesidad de verificar
                        # si ya existe el email en la base de datos, y
                        # solo se actualiza la password
                        if 'password' in request.data:
                            if len(request.data['password']) == 0:
                                print " password esta vacio"
                            else:
                                user.password = make_password(request.data['password'])
                        user.name = request.data['name']
                        user.save()
                        return Response({
                            'mensaje': 'Actualizado correctamente.'},
                            status=status.HTTP_201_CREATED
                        )
                    else:
                        # si los emails no son iguales, hay que preguntar si el email nuevo esta siendo utilizado
                        # por otro usuario.
                        try:
                            userq = User.objects.get(email=request.data['email'])
                            return Response({'error': 'El email proporcionado ya esta siendo utilizado por otro usuario'})

                        except ObjectDoesNotExist:
                            # si no existe, el email esta libre
                            # y puede ser utilizado.
                            user.email = request.data['email']
                            if 'password' in request.data:
                                if len(request.data['password']) == 0:
                                    print "password esta vacio"
                                else:
                                    user.password = make_password(request.data['password'])
                            user.name = request.data['name']
                            user.save()
                            return Response({
                                'mensaje': 'Actualizado correctamente.'},
                                status=status.HTTP_201_CREATED
                            )
                else:
                    return Response({
                        'error': 'El correo no tiene formato requerido, verifique el correo, eje: correo@dominio.com '}
                    )
        else:
            return Response({'error': 'No se recibieron parametros'})


@api_view(['GET', 'POST'])
@permission_classes((permissions.IsAuthenticated, ))
def direcciones(request):
    """
    Lista de las direcciones del usuario, y creacion de nuevas, es necesario estar autenticado.
    """
    if request.method == 'GET':
        direccion = Direccion.objects.filter(user=request.user)
        serializer = DireccionSerializer(direccion, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = DireccionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated, ))
def productos(request):
    """
    Lista de los productos que estan en la base de datos.
    """
    if request.method == 'GET':
        producto = Producto.objects.all()
        serializer = ProductoSerializer(producto, many=True)
        return Response(serializer.data)


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated, ))
def producto(request, pk):
    """
    Detalle de un producto.
    """
    try:
        producto = Producto.objects.get(pk=pk)
    except Producto.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = ProductoSerializer(producto)
        return Response(serializer.data)


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated, ))
def tipos(request):
    """
    Lista de los tipos de condones.
    """
    if request.method == 'GET':
        tipo = Tipo.objects.all()
        serializer = TipoSerializer(tipo, many=True)
        return Response(serializer.data)


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated, ))
def tipo(request, pk):
    """
    Detalle de un tipo de condon.
    """
    try:
        tipo = Tipo.objects.get(pk=pk)
    except Tipo.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = TipoSerializer(tipo)
        return Response(serializer.data)


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated, ))
def marcas(request):
    """
    Lista de las marcas de condones.
    """
    if request.method == 'GET':
        marca = Marca.objects.all()
        serializer = MarcaSerializer(marca, many=True)
        return Response(serializer.data)


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated, ))
def marca(request, pk):
    """
    Detalle de una marca.
    """
    try:
        marca = Marca.objects.get(pk=pk)
    except Marca.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = MarcaSerializer(marca)
        return Response(serializer.data)


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated, ))
def misServicios(request):
    """
    Lista de servicios del usuario, es necesario estar autenticado.
    """
    if request.method == 'GET':
        servicio = Servicio.objects.filter(user=request.user)
        serializer = ServicioDetailSerializer(servicio, many=True)
        return Response(serializer.data)


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated, ))
def QuiicklerServicios(request):
    """
    Lista de servicios del quiickler, es necesario estar autenticado.
    """
    if request.method == 'GET':
        servicio = Servicio.objects.filter(quiickler__user=request.user)
        serializer = ServicioDetailSerializer(servicio, many=True)
        return Response(serializer.data)


@api_view(['GET'])
@permission_classes((permissions.AllowAny, ))
def QuiicklersHaversine(request, lat, lng):
    config = Configuration.objects.get()
    if request.method == 'GET':
        lat = float(lat)
        lng = float(lng)
        RegistroBusqueda.objects.create(user=request.user, longitud=lng, latitud=lat)
        distance = config.area_busqueda
        box = getBoundaries(lat, lng, distance)
        query = "select *,(6371 * ACOS(COS( RADIANS(%f))*COS(RADIANS(latitud))*COS(RADIANS(longitud)-RADIANS(%f))+SIN(RADIANS(%f))*SIN(RADIANS(latitud)))) AS distance FROM dashboard_quiickler WHERE (latitud BETWEEN %f AND %f) AND (longitud BETWEEN %f AND %f) AND estado = 1 HAVING distance < %i ORDER BY distance ASC" % (lat, lng, lat, box['min_lat'], box['max_lat'], box['min_lng'], box['max_lng'], distance)
        quiickler = Quiickler.objects.raw(query)
        serializer = QuiicklerSerializer(quiickler, many=True)
        return Response(serializer.data)


@api_view(['POST'])
@permission_classes((permissions.IsAuthenticated, ))
def servicioExpress(request):
    """
    Crear un servicio Express.
    """
    if request.method == 'POST':
        if 'pedido' in request.data and 'servicio' in request.data:
            print "no estan"
        else:
            return Response(
                {"error": "faltan el pedido o el servicio"},
                status=status.HTTP_400_BAD_REQUEST
            )
        serializer = PedidoCreateSerializer(data=request.data['pedido'])
        servicioSerializer = ServicioCreateSerializer(
            data=request.data['servicio']
        )

        if 'productos' in request.data['pedido']:
            if len(request.data['pedido']['productos']) > 0:
                try:
                    for prod in request.data['pedido']['productos']:
                        p = Producto.objects.get(pk=prod['producto'])
                        if prod['cantidad'] <= p.cantidad:
                            pass
                        else:
                            return Response(
                                {
                                "mensaje": "No tenemos productos en stock, para la cantidad solicitada",
                                "producto": {"nombre": p.nombre, "id": p.pk, "cantidad": p.cantidad},
                                "pedido": {"producto_id": p.pk, "cantidad": prod['cantidad']}
                                },
                                status=status.HTTP_400_BAD_REQUEST
                            )
                except Producto.DoesNotExist:
                    return Response(
                        {
                        "error": "Uno de los productos no se encontro en la base de datos"
                        }, status=status.HTTP_400_BAD_REQUEST
                    )
                if serializer.is_valid() and servicioSerializer.is_valid():
                    pedido = serializer.save()
                    servicio = servicioSerializer.save(pedido=pedido, total=pedido.total)
                    # la serializacion se hace manual por que se necesita mostrar los productos
                    # que se pidieron mas la cantidad en el pedido
                    productos = [{"producto": item.producto.id, "cantidad": item.cantidad} for item in pedido.productopedido_set.all()]
                    return Response({"mensaje": "El servicio se creo correctamente!.", "id": servicio.id}, status=status.HTTP_201_CREATED)
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"error": "No se encontraron productos"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"error": "No se encontraron productos"}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes((permissions.IsAuthenticated, ))
def servicioProgramado(request):
    """
    Crear un servicio Express.
    """
    if request.method == 'POST':
        if 'pedido' in request.data and 'servicio' in request.data:
            pass
        else:
            return Response({
                        "error": "faltan el pedido o el servicio"},
                        status=status.HTTP_400_BAD_REQUEST
                    )
        serializer = PedidoCreateSerializer(data=request.data['pedido'])
        servicioSerializer = ServicioCreateSerializer(data=request.data['servicio'])

        if 'productos' in request.data['pedido']:
            if len(request.data['pedido']['productos']) > 0:
                try:
                    for prod in request.data['pedido']['productos']:
                        p = Producto.objects.get(pk=prod['producto'])
                        if prod['cantidad'] <= p.cantidad:
                            pass
                        else:
                            return Response(
                                {
                                "mensaje": "No tenemos productos en stock, para la cantidad solicitada",
                                "producto": {"nombre": p.nombre, "id": p.pk, "cantidad": p.cantidad},
                                "pedido": {"producto_id": p.pk, "cantidad": prod['cantidad']}
                                },
                                status=status.HTTP_400_BAD_REQUEST
                            )
                except Producto.DoesNotExist:
                    return Response(
                        {
                        "error": "Uno de los productos no se encontro en la base de datos"
                        }, status=status.HTTP_400_BAD_REQUEST
                    )
                if serializer.is_valid() and servicioSerializer.is_valid():
                    pedido = serializer.save()
                    servicio = servicioSerializer.save(pedido=pedido, tipo='programado')
                    # la serializacion se hace manual por que se necesita mostrar los productos
                    # que se pidieron mas la cantidad en el pedido

                    productos = [{"producto": item.producto.id, "cantidad": item.cantidad} for item in pedido.productopedido_set.all()]
                    return Response({"mensaje": "El servicio se creo correctamente!.", "id": servicio.id}, status=status.HTTP_201_CREATED)
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response({"error": "No se encontraron productos"}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"error": "No se encontraron productos"}, status=status.HTTP_400_BAD_REQUEST)



@api_view(['GET', 'PUT'])
@permission_classes((permissions.IsAuthenticated, ))
def pedidoElemento(request, pk):
    """
    Detalle de un pedido.
    """

    try:
        pedido = Pedido.objects.get(pk=pk)
    except Pedido.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        productos = [{"producto": item.producto.id, "cantidad": item.cantidad} for item in pedido.productopedido_set.all()]
        return Response({
            "id": pedido.id,
            "user": pedido.user.pk,
            "created": pedido.created.strftime("%Y-%m-%d %H:%M:%S") if pedido.created else None,
            "productos": productos,
            "fecha_confirmacion": pedido.fecha_confirmacion.strftime("%Y-%m-%d %H:%M:%S") if pedido.fecha_confirmacion else None,
            "direccion": {"id": pedido.direccion.id,
                        "nombre": pedido.direccion.nombre,
                        "direccion": pedido.direccion.direccion},
            "estado": pedido.estado,
            "total": pedido.total,
            "longitud": pedido.longitud,
            "latitud": pedido.latitud}, status=status.HTTP_200_OK)

    elif request.method == 'PUT':
        serializer = PedidoSerializer(pedido, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT'])
@permission_classes((permissions.IsAuthenticated, ))
def servicioElemento(request, pk):
    """
    Detalle de un servicio.
    """

    try:
        servicio = Servicio.objects.get(pk=pk)
    except Servicio.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = ServicioDetailSerializer(servicio)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = PedidoSerializer(pedido, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['PUT'])
@permission_classes((permissions.IsAuthenticated, ))
def confirmarServicio(request, pk):
    """
    Confirmar servicio.
    """

    try:
        servicio = Servicio.objects.get(pk=pk)
    except Servicio.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'PUT':
        if servicio.quiickler:
            return Response({
                "mensaje": "Ya el servicio esta siendo tramitado por un quiickler"},
                status=status.HTTP_400_BAD_REQUEST
            )
        if servicio.estado == 'sinrespuesta':
            return Response({
                "mensaje": "El servicio fue cancelado, por la tardanza en la respuesta de los quiicklers"},
                status=status.HTTP_400_BAD_REQUEST
            )
        if 'respuesta' in request.data:
            if request.data['respuesta']:
                quiickler = get_object_or_404(
                    Quiickler,
                    user__pk=request.user.pk
                )
                servicio.quiickler = quiickler
                # Buscar dispositivos android registrados con el id del usuario
                devices_user = GCMDevice.objects.filter(user=servicio.user)
                servicio.estado = 'espera'
                servicio.fecha_confirmacion = datetime.datetime.now()
                servicio.save()
                quiickler.estado = False
                quiickler.save()
                # Enviar notificacion (Bulk)
                res = devices_user.send_message(
                    None,
                    extra={
                        "servicio_id": "%s" % servicio.pk,
                        "mensaje": "Pedido confirmado",
                        "tipo": "servicio",
                        "estado": "%s" % servicio.estado}
                )
                print res
                return Response({
                    "mensaje": "servicio confirmado"},
                    status=status.HTTP_200_OK
                )
        else:
            return HttpResponse(status=404)


@api_view(['PUT'])
@permission_classes((permissions.IsAuthenticated, ))
def servicioFinalizado(request, pk):
    """
    Finalizar un servicio.
    """

    try:
        servicio = Servicio.objects.get(pk=pk)
    except Servicio.DoesNotExist:
        return Response({
            "error": "servicio no encontrado"},
            status=status.HTTP_400_BAD_REQUEST
        )

    try:
        quiickler = Quiickler.objects.get(user=request.user)
    except Quiickler.DoesNotExist:
        return Response(
            {"error": "acceso denegado!."},
            status=status.HTTP_400_BAD_REQUEST
        )

    if request.method == 'PUT':
        if 'estado' in request.data:
            if not servicio.quiickler:
                print "No hay quiickler"
                return Response({
                    "error": "quiickler sin asignar"},
                    status=status.HTTP_400_BAD_REQUEST
                )
            if quiickler.pk != servicio.quiickler.pk:
                print "no son iguales los quiicklers"
                return Response(
                    {"error": "acceso denegado!."},
                    status=status.HTTP_400_BAD_REQUEST
                )

            if request.data['estado'] == 'finalizar':
                if servicio.estado != 'entregado':
                    servicio.estado = 'entregado'
                    servicio.fecha_entrega = datetime.datetime.now()
                    if 'gratis' in request.data:
                        if request.data['gratis'] == 'si':
                            servicio.gratis = True
                    servicio.save()
                    quiickler.estado = True
                    quiickler.save()
                return Response(
                    {"mensaje": "servicio finalizado satisfactoriamente"},
                    status=status.HTTP_200_OK
                )
            else:
                return Response({
                    "error": "revisa la documentacion de la api, debes colocar un atributo"},
                    status=status.HTTP_400_BAD_REQUEST
                )
        else:
            return Response({
                "error": "no se encontraron datos"},
                status=status.HTTP_400_BAD_REQUEST
            )


@api_view(['PUT'])
@permission_classes((permissions.IsAuthenticated, ))
def rechazarServicio(request, pk):
    """
    Rechazar servicio.
    """

    try:
        servicio = Servicio.objects.get(pk=pk)
    except Servicio.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'PUT':
        if 'respuesta' in request.data:
            if request.data['respuesta'] == 'rechazar':
                quiickler = get_object_or_404(
                    Quiickler,
                    user__pk=request.user.pk
                )
                try:
                    notificacion_registro = NotificacionServicio.objects.get(
                        servicio=servicio,
                        quiickler=quiickler
                    )
                except ObjectDoesNotExist:
                    return Response({
                        "error": "el quiickler no fue encontrado"},
                        status=status.HTTP_400_BAD_REQUEST
                    )
                notificacion_registro.estado = 'rechazado'
                notificacion_registro.save()
                return Response({
                    "mensaje": "servicio rechazado"},
                    status=status.HTTP_200_OK
                )
        else:
            return HttpResponse(status=404)


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated, ))
def matarServicio(request, pk):
    """
    Matar servicio, sucede cuando pasan 2 minutos del envio del servicio,
    y los quiicklers no responden.
    """

    try:
        servicio = Servicio.objects.get(pk=pk)
        if servicio.estado != 'confirmando':
            return Response({
                "error": "El servicio debe estar en el estado confirmado"},
                status=status.HTTP_400_BAD_REQUEST
            )
    except Servicio.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        if servicio.quiickler:
            return Response({
                "error": "No se puede matar el servicio, porque ya tiene asigando un quiickler"},
                status=status.HTTP_400_BAD_REQUEST
            )
        else:
            servicio.estado = 'sinrespuesta'
            servicio.save()
            return Response({
                "mensaje": "El pedido fue cancelado"},
                status=status.HTTP_200_OK
            )


@api_view(['GET', 'PUT'])
@permission_classes((permissions.IsAuthenticated, ))
def quiickler(request):
    """
    Detalle del quiickler.
    """

    try:
        q = Quiickler.objects.get(user=request.user)
    except Quiickler.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = QuiicklerSerializer(q)
        n_pedidos = Servicio.objects.filter(quiickler=q).count()
        pagos = Pago.objects.filter(quiickler=q)
        recibido = 0
        debe = 0
        for pago in pagos:
            recibido += pago.servicio.total
            if not pago.estado:
                print "debe plata"
                debe += pago.servicio.total
        return Response({
            "numero_pedidos": n_pedidos,
            "recibido": recibido,
            "debe": debe,
            "quiickler": serializer.data}
        )

    elif request.method == 'PUT':
        serializer = QuiicklerSerializer(q, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated, ))
def quiicklerElemento(request, pk):
    """
    Detalle de un quiickler.
    """
    try:
        quiickler = Quiickler.objects.get(pk=pk)
    except Quiickler.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = QuiicklerSerializer(quiickler)
        return Response(serializer.data)


@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes((permissions.IsAuthenticated, ))
def direccion(request, pk):
    """
    Detalle de una direccion del usuario, y eliminar,
    es necesario estar autenticado.
    """

    try:
        direccion = Direccion.objects.get(pk=pk, user=request.user)
    except Direccion.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = DireccionSerializer(direccion)
        return Response(serializer.data)

    if request.method == 'DELETE':
        direccion.delete()
        return Response(
            {"response": "Eliminado"},
            status=status.HTTP_204_NO_CONTENT
        )

    elif request.method == 'PUT':
        serializer = DireccionSerializer(direccion, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
@permission_classes((permissions.IsAuthenticated, ))
def evaluaciones(request):
    """
    Lista de evaluaciones, y creacion de nuevas, es necesario estar autenticado.
    """
    if request.method == 'GET':
        evaluacion = Evaluacion.objects.filter(user=request.user)
        serializer = EvaluacionSerializer(evaluacion, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = EvaluacionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


from base64 import b64decode
from django.core.files.base import ContentFile
from time import time


@api_view(['POST'])
@permission_classes((permissions.IsAuthenticated, ))
def cambiarImagenUsuario(request):
    """
    Cambiar la imagen del usuario.
    """

    if request.method == 'POST':
        if 'imagen' in request.data:
            b64_text = request.data['imagen']
            image_data = b64decode(b64_text)
            filename = "img%s.png" % str(time()).replace('.', '_')
            try:
                profile = Profile.objects.get(user=request.user)
            except Profile.DoesNotExist:
                return Response(
                    {"error": "No se encontro el usuario"},
                    status=status.HTTP_400_BAD_REQUEST
                )
            profile.picture = ContentFile(image_data, filename)
            profile.save()
            return Response(
                {"mensaje": "se actualizo correctamente", "imagen_url": profile.picture.url},
                status=status.HTTP_200_OK
            )
        else:
            return Response(
                {"errror": "No se encontro la imagen"},
                status=status.HTTP_400_BAD_REQUEST
            )
