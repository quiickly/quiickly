from django.conf.urls import url, include
from rest_framework import routers

from .views import *

router = routers.DefaultRouter()

urlpatterns = [
    url(r'^v1/api-login', ObtainAuthToken.as_view()),
    url(r'^v1/quiickler/login', ObtainAuthTokenQuiickler.as_view()),
    url(r'^v1/quiickler/$', 'api.views.quiickler'),
    url(r'^v1/quiickler/(?P<pk>[0-9]+)/$', 'api.views.quiicklerElemento'),
    url(r'^v1/quiickler/disponibles/(-?\d+(?:\.\d+)?)/(-?\d+(?:\.\d+)?)/$', 'api.views.QuiicklersHaversine'),
    url(r'^v1/quiickler/historial/$', 'api.views.QuiicklerServicios'),
    url(r'^v1/servicio/express/$', 'api.views.servicioExpress'),
    url(r'^v1/servicio/programado/$', 'api.views.servicioProgramado'),
    url(r'^v1/servicio/sinrespuesta/(?P<pk>[0-9]+)/$', 'api.views.matarServicio'),
    url(r'^v1/servicio/confirmar/(?P<pk>[0-9]+)/$', 'api.views.confirmarServicio'),
    url(r'^v1/servicio/rechazar/(?P<pk>[0-9]+)/$', 'api.views.rechazarServicio'),
    url(r'^v1/servicio/(?P<pk>[0-9]+)/$', 'api.views.servicioElemento'),
    url(r'^v1/servicio/historial/$', 'api.views.misServicios'),
    #url(r'^v1/pedidos/$', 'api.views.pedidos'),
    url(r'^v1/servicio/finalizar/(?P<pk>[0-9]+)/$', 'api.views.servicioFinalizado'),
    url(r'^v1/evaluaciones/$', 'api.views.evaluaciones'),
    url(r'^v1/productos/$', 'api.views.productos'),
    url(r'^v1/productos/(?P<pk>[0-9]+)/$', 'api.views.producto'),
    url(r'^v1/tipos/$', 'api.views.tipos'),
    url(r'^v1/tipos/(?P<pk>[0-9]+)/$', 'api.views.tipo'),
    url(r'^v1/marcas/$', 'api.views.marcas'),
    url(r'^v1/marcas/(?P<pk>[0-9]+)/$', 'api.views.marca'),
    url(r'^v1/usuarios/detalle/', 'api.views.detalleUser'),
    url(r'^v1/usuarios/registrar', 'api.views.createUser'),
    url(r'^v1/usuarios/actualizar', 'api.views.actualizarDatos'),
    url(r'^v1/usuarios/direccion/$', 'api.views.direcciones'),
    url(r'^v1/usuarios/direccion/(?P<pk>[0-9]+)/$', 'api.views.direccion'),
    url(r'^v1/usuarios/recuperarpassword', 'api.views.olvidePassword'),
    url(r'^v1/usuarios/imagen/', 'api.views.cambiarImagenUsuario'),
    url(r'^v1/notificacion/registrar/android/', 'api.views.registrarAndroid'),
    url(r'^v1/', include(router.urls)),
]
