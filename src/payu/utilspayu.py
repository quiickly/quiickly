# -*- coding: utf-8 -*-
from hashlib import md5
from decimal import Decimal, ROUND_HALF_EVEN


def generate_hash_payu(api_key, mId, rfcode, amount, currency):
    # parametros
    # “ApiKey~merchantId~referenceCode~amount~currency”.
    text = api_key + '~' + mId + '~' + rfcode + '~' + str(amount) + '~' + currency
    hash = md5(text)
    return hash.hexdigest().lower()


def generate_hash_confirmation_payu(api_key, mId, rfsale, new_value, currency, state_pol):
    # parametros
    # "ApiKey~merchant_id~reference_sale~new_value~currency~state_pol".
    print "******generando hash de confirmacion********"
    print "api_key: ", api_key
    print "mId: ", mId
    print "rfsale: ", rfsale
    print "new_value: ", new_value
    print "currency: ", currency
    print "state_pol: ", state_pol
    text = str(api_key) + '~' + str(mId) + '~' + rfsale + '~' + str(new_value) + '~' + currency + '~' + str(state_pol)
    hash = md5(text)
    sign = hash.hexdigest().lower()
    print "sign generada: ", sign
    return sign


def payu_number_format(num, places=1):
    places_resul = Decimal(10) ** -places
    a = Decimal(str(num))
    return a.quantize(Decimal(places_resul), rounding=ROUND_HALF_EVEN)


def payu_number_format_confirmation(num):
    two_digit = "%.2f" % round(num, 2)
    decimals = str(two_digit)[-3:]
    last_position = str(decimals)[2:]
    print "decimales : ", decimals
    print "ultima posicion: ", last_position
    if int(last_position) == 0:
        print "ultima posicion es igual a 0"
        return "%.1f" % round(num, 1)
    else:
        places_resul = Decimal(10) ** -2
        a = Decimal(str(num))
        return a.quantize(Decimal(places_resul), rounding=ROUND_HALF_EVEN)





# def verify_hash(data, SALT):
#     keys.reverse()
#     hash = sha512(settings.PAYU_INFO.get('merchant_salt'))
#     hash.update("%s%s" % ('|', str(data.get('status', ''))))
#     for key in KEYS:
#         hash.update("%s%s" % ('|', str(data.get(key, ''))))
#     return (hash.hexdigest().lower() == data.get('hash'))
