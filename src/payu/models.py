from django.db import models
from accounts.models import TimeStampedModel
from django.conf import settings
from solo.models import SingletonModel


TRANSACTIONSTATE = (
    ('APPROVED', 'APPROVED'),
    ('DECLINED', 'DECLINED'),
    ('ERROR', 'ERROR'),
    ('EXPIRED', 'EXPIRED'),
    ('PENDING', 'PENDING')
)


class Orden(TimeStampedModel):
    """ Modelo para las ordenes """

    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    merchantId = models.CharField(max_length=12)
    lapTransactionState = models.CharField(
        max_length=64,
        choices=TRANSACTIONSTATE,
        default='PENDING'
    )
    transactionState = models.IntegerField(null=True, blank=True)
    referenceCode = models.CharField(max_length=50)
    lapResponseCode = models.CharField(max_length=64, null=True, blank=True)
    reference_pol = models.CharField(max_length=255, null=True, blank=True)
    signature = models.CharField(max_length=255)
    lapPaymentMethod = models.CharField(max_length=255, null=True, blank=True)
    TX_VALUE = models.FloatField()
    TX_TAX = models.FloatField()
    buyerEmail = models.EmailField()
    currency = models.CharField(max_length=3)
    message = models.CharField(max_length=255, null=True, blank=True)
    transactionId = models.CharField(max_length=36, null=True, blank=True)
    processingDate = models.DateTimeField(null=True, blank=True)

    def __unicode__(self):
        return '%s: %s' % (self.lapTransactionState, self.TX_VALUE)


class Configuracion(SingletonModel):
    test = models.BooleanField(default=True)
    account_id_test = models.CharField(
        "id cuenta(test)",
        max_length=50,
        default="500538"
    )
    merchant_id_test = models.CharField(
        "id comercio(test)",
        max_length=50,
        default="500238"
    )
    api_key_test = models.CharField(
        "Api key(test)",
        max_length=250,
        default="6u39nqhq8ftd0hlvnjfs66eh8c"
    )
    url_test = models.CharField(
        "Url test",
        max_length=50,
        default="https://stg.gateway.payulatam.com/ppp-web-gateway"
    )
    url_success_test = models.CharField(
        "Url success test",
        max_length=50,
        default="http://54.187.26.141:8000/web/orden/"
    )
    url_confirmation_test = models.CharField(
        "Url confirmation test",
        max_length=50,
        default="http://54.187.26.141:8000/web/confirmacion/"
    )
    merchant_id = models.CharField(
        "id comercio",
        max_length=50,
        default="500238"
    )
    account_id = models.CharField("id cuenta", max_length=50, default="500538")
    currency = models.CharField("Moneda", max_length=4, default="COP")
    api_key = models.CharField(
        "Api key",
        max_length=250,
        default="6u39nqhq8ftd0hlvnjfs66eh8c"
    )
    public_key = models.CharField(
        "Public key",
        max_length=250,
        default="6u39nqhq8ftd0hlvnjfs66eh8c"
    )
    api_login = models.CharField(
        "Api login",
        max_length=250,
        default="vrVeIj6VkdB7gvU"
    )
    url_produccion = models.CharField(
        "Url produccion(payu)",
        max_length=50,
        default="https://gateway.payulatam.com/ppp-web-gateway"
    )
    url_success_prod = models.CharField(
        "Url success produccion",
        max_length=50,
        default="http://54.187.26.141:8000/web/orden/"
    )
    url_confirmation_prod = models.CharField(
        "Url confirmation produccion",
        max_length=50,
        default="http://54.187.26.141:8000/web/confirmacion/"
    )

    def __unicode__(self):
        return u"Variables de payu"

    class Meta:
        verbose_name = "Variables de entorno(payu)"
