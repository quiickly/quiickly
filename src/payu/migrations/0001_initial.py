# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Orden',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('merchantId', models.CharField(max_length=12)),
                ('lapTransactionState', models.CharField(max_length=64, choices=[(b'APPROVED', b'APPROVED'), (b'DECLINED', b'DECLINED'), (b'ERROR', b'ERROR'), (b'EXPIRED', b'EXPIRED'), (b'PENDING', b'PENDING')])),
                ('transactionState', models.IntegerField()),
                ('referenceCode', models.CharField(max_length=50)),
                ('lapResponseCode', models.CharField(max_length=64)),
                ('reference_pol', models.CharField(max_length=255)),
                ('signature', models.CharField(max_length=255)),
                ('TX_VALUE', models.FloatField()),
                ('TX_TAX', models.FloatField()),
                ('buyerEmail', models.EmailField(max_length=254)),
                ('currency', models.CharField(max_length=3)),
                ('lapPaymentMethod', models.CharField(max_length=255)),
                ('message', models.CharField(max_length=255)),
                ('transactionId', models.CharField(max_length=36)),
                ('processingDate', models.DateTimeField()),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
