# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('payu', '0003_auto_20151207_1624'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orden',
            name='processingDate',
            field=models.DateField(null=True, blank=True),
        ),
    ]
