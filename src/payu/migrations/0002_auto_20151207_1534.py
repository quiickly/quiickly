# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('payu', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orden',
            name='lapPaymentMethod',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='orden',
            name='lapResponseCode',
            field=models.CharField(max_length=64, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='orden',
            name='lapTransactionState',
            field=models.CharField(default=b'PENDING', max_length=64, choices=[(b'APPROVED', b'APPROVED'), (b'DECLINED', b'DECLINED'), (b'ERROR', b'ERROR'), (b'EXPIRED', b'EXPIRED'), (b'PENDING', b'PENDING')]),
        ),
        migrations.AlterField(
            model_name='orden',
            name='message',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='orden',
            name='processingDate',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='orden',
            name='reference_pol',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='orden',
            name='transactionId',
            field=models.CharField(max_length=36, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='orden',
            name='transactionState',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
