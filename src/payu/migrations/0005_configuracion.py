# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('payu', '0004_auto_20151215_1101'),
    ]

    operations = [
        migrations.CreateModel(
            name='Configuracion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('test', models.BooleanField(default=True)),
                ('account_id_test', models.CharField(default=b'500538', max_length=50, verbose_name=b'id cuenta(test)')),
                ('merchant_id_test', models.CharField(default=b'500238', max_length=50, verbose_name=b'id comercio(test)')),
                ('api_key_test', models.CharField(default=b'6u39nqhq8ftd0hlvnjfs66eh8c', max_length=250, verbose_name=b'Api key(test)')),
                ('url_test', models.CharField(default=b'https://stg.gateway.payulatam.com/ppp-web-gateway', max_length=50, verbose_name=b'Url test')),
                ('url_success_test', models.CharField(default=b'http://54.187.26.141:8000/web/orden/', max_length=50, verbose_name=b'Url success test')),
                ('url_confirmation_test', models.CharField(default=b'http://54.187.26.141:8000/web/confirmacion/', max_length=50, verbose_name=b'Url confirmation test')),
                ('merchant_id', models.CharField(default=b'500238', max_length=50, verbose_name=b'id comercio')),
                ('account_id', models.CharField(default=b'500538', max_length=50, verbose_name=b'id cuenta')),
                ('currency', models.CharField(default=b'COP', max_length=4, verbose_name=b'Moneda')),
                ('api_key', models.CharField(default=b'6u39nqhq8ftd0hlvnjfs66eh8c', max_length=250, verbose_name=b'Api key')),
                ('public_key', models.CharField(default=b'6u39nqhq8ftd0hlvnjfs66eh8c', max_length=250, verbose_name=b'Public key')),
                ('api_login', models.CharField(default=b'vrVeIj6VkdB7gvU', max_length=250, verbose_name=b'Public key')),
                ('url_produccion', models.CharField(default=b'https://gateway.payulatam.com/ppp-web-gateway', max_length=50, verbose_name=b'Url produccion(payu)')),
                ('url_success_prod', models.CharField(default=b'http://54.187.26.141:8000/web/orden/', max_length=50, verbose_name=b'Url success produccion')),
                ('url_confirmation_prod', models.CharField(default=b'http://54.187.26.141:8000/web/confirmacion/', max_length=50, verbose_name=b'Url confirmation produccion')),
            ],
            options={
                'verbose_name': 'Variables de entorno(payu)',
            },
        ),
    ]
