# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('payu', '0006_auto_20151217_1537'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orden',
            name='processingDate',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
