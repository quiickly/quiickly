# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('payu', '0005_configuracion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='configuracion',
            name='api_login',
            field=models.CharField(default=b'vrVeIj6VkdB7gvU', max_length=250, verbose_name=b'Api login'),
        ),
    ]
