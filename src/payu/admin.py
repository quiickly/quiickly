from django.contrib import admin
from .models import *
from solo.admin import SingletonModelAdmin

admin.site.register(Configuracion, SingletonModelAdmin)


@admin.register(Orden)
class OrdenAdmin(admin.ModelAdmin):
    list_display = ('created', 'modified', 'user', 'lapTransactionState', 'TX_VALUE')
