from .models import Configuracion

payuVar = Configuracion.get_solo()


PAYU_INFO = {
    'api_key': payuVar.api_key_test if payuVar.test else payuVar.api_key,
    'merchantId': payuVar.merchant_id_test if payuVar.test else payuVar.merchant_id,
    'accountId': payuVar.account_id_test if payuVar.test else payuVar.account_id,
    'api_login': payuVar.api_login,
    'currency': payuVar.currency,
    'public_key': payuVar.public_key,
    'server': payuVar.url_test if payuVar.test else payuVar.url_produccion,
    'surl': payuVar.url_success_test if payuVar.test else payuVar.url_success_prod,
    'curl': payuVar.url_confirmation_test if payuVar.test else payuVar.url_confirmation_prod,
    'test': '1' if payuVar.test else '0',
}
