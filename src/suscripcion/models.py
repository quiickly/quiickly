from django.db import models
from django.conf import settings
from accounts.models import TimeStampedModel
from dashboard.models import Producto
from cities_light.models import *
from django.db.models.signals import pre_save, pre_delete, post_save, post_delete
from django.dispatch import receiver
from payu.models import Orden


class Paquete(TimeStampedModel):
    """ Modelo para los paquetes de suscripcion """

    nombre = models.CharField('Nombre del paquete', max_length=50)
    productos = models.ManyToManyField(Producto)
    precio = models.FloatField()
    cantidad = models.IntegerField()
    descripcion = models.TextField()

    def __unicode__(self):
        return '%s: %s' % (self.nombre, self.cantidad)


class Direccion(TimeStampedModel):
    """ Modelo para direccion de envios"""

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name='direccion_envios')

    direccion = models.CharField(max_length=70)
    ciudad = models.ForeignKey(City)

    def __unicode__(self):
        return '%s' % self.direccion


class Suscripcion(TimeStampedModel):
    """ Modelo para las suscripciones """

    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    paquete = models.ForeignKey(Paquete)
    fecha_entrega = models.DateField('Fecha de entrega')
    estado = models.BooleanField(default=True)
    valor = models.FloatField()
    direccion = models.OneToOneField(Direccion, null=True)

    def __unicode__(self):
        return '%s: %s' % (self.user.email, self.paquete.nombre)


ESTADOENVIO = (
    ('PROGRAMADO', 'PROGRAMADO'),
    ('DESPACHADO', 'DESPACHADO'),
    ('NOENVIADO', 'NOENVIADO')
)


class Empresa(TimeStampedModel):
    """ Modelo para las empresas de transporte """

    nombre = models.CharField(max_length=50)
    telefono = models.CharField(max_length=50)
    pagina = models.URLField()

    def __unicode__(self):
        return '%s: %s' % (self.nombre, self.telefono)


class Envio(TimeStampedModel):
    """ Modelo para los envios de la tienda """

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
    )
    empresa = models.ForeignKey(Empresa, null=True, blank=True)
    estado = models.CharField(
        "Estado del envio",
        max_length=50,
        choices=ESTADOENVIO,
        default="PROGRAMADO"
    )
    guia = models.CharField(
        "guia de envio",
        max_length=250,
        default="No disponible"
    )
    paquete = models.ForeignKey(Paquete)
    fecha_envio_programada = models.DateTimeField()
    orden = models.OneToOneField(Orden, null=True, blank=True)
    direccion = models.CharField(max_length=70, null=True)
    ciudad = models.ForeignKey(City, null=True)

    def __unicode__(self):
        return '%s: %s - %s' % (
            self.user.email,
            self.estado,
            self.paquete.descripcion
        )


@receiver(post_save, sender=Orden)
def pedido_post_save(sender, instance, created, **kwargs):
    print "############# ORDEN POST SAVE ###################"
    if instance:
        if instance.lapTransactionState == 'APPROVED':
            suscripcion = instance.user.suscripcion
            envio = Envio()
            envio.orden = instance
            envio.user = instance.user
            envio.estado = 'PROGRAMADO'
            envio.paquete = suscripcion.paquete
            envio.fecha_envio_programada = suscripcion.fecha_entrega
            envio.direccion = suscripcion.direccion.direccion
            envio.ciudad = suscripcion.direccion.ciudad
            envio.save()
