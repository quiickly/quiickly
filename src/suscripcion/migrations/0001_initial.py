# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('dashboard', '0025_auto_20151202_1117'),
    ]

    operations = [
        migrations.CreateModel(
            name='Paquete',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('nombre', models.CharField(max_length=50, verbose_name=b'Nombre del paquete')),
                ('precio', models.FloatField()),
                ('cantidad', models.IntegerField()),
                ('descripcion', models.TextField()),
                ('productos', models.ManyToManyField(to='dashboard.Producto')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Suscripcion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('fecha_entrega', models.DateField(verbose_name=b'Fecha de entrega')),
                ('estado', models.BooleanField(default=True)),
                ('valor', models.FloatField()),
                ('paquete', models.OneToOneField(to='suscripcion.Paquete')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
