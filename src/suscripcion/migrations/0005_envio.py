# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('payu', '0006_auto_20151217_1537'),
        ('suscripcion', '0004_auto_20151203_2158'),
    ]

    operations = [
        migrations.CreateModel(
            name='Envio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('estado', models.CharField(default=b'PROGRAMADO', max_length=50, verbose_name=b'Estado del envio', choices=[(b'PROGRAMADO', b'PROGRAMADO'), (b'DESPACHADO', b'DESPACHADO'), (b'NOENVIADO', b'NOENVIADO')])),
                ('guia', models.CharField(default=b'No disponible', max_length=250, verbose_name=b'guia de envio')),
                ('fecha_envio_programada', models.DateTimeField()),
                ('orden', models.OneToOneField(null=True, blank=True, to='payu.Orden')),
                ('paquete', models.ForeignKey(to='suscripcion.Paquete')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
