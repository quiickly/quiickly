# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('suscripcion', '0002_direccion'),
    ]

    operations = [
        migrations.AddField(
            model_name='suscripcion',
            name='direccion',
            field=models.OneToOneField(null=True, to='suscripcion.Direccion'),
        ),
    ]
