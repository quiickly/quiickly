# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('suscripcion', '0006_auto_20151217_1946'),
    ]

    operations = [
        migrations.AlterField(
            model_name='envio',
            name='direccion',
            field=models.CharField(max_length=70, null=True),
        ),
    ]
