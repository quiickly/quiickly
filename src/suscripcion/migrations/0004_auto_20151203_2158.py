# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('suscripcion', '0003_suscripcion_direccion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='suscripcion',
            name='paquete',
            field=models.ForeignKey(to='suscripcion.Paquete'),
        ),
    ]
