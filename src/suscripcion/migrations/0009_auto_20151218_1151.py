# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('suscripcion', '0008_auto_20151218_1047'),
    ]

    operations = [
        migrations.AlterField(
            model_name='envio',
            name='empresa',
            field=models.ForeignKey(blank=True, to='suscripcion.Empresa', null=True),
        ),
    ]
