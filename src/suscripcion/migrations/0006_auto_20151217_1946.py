# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('suscripcion', '0005_envio'),
    ]

    operations = [
        migrations.AddField(
            model_name='envio',
            name='ciudad',
            field=models.ForeignKey(to='cities_light.City', null=True),
        ),
        migrations.AddField(
            model_name='envio',
            name='direccion',
            field=models.TextField(null=True),
        ),
    ]
