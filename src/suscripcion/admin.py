from django.contrib import admin
from .models import *


@admin.register(Suscripcion, Direccion, Empresa)
class DashSuscripcionAdmin(admin.ModelAdmin):
    pass


@admin.register(Paquete)
class PaqueteAdmin(admin.ModelAdmin):
    list_display = (
        'nombre',
        'precio',
        'cantidad',
        'created'
    )


@admin.register(Envio)
class EnvioAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'estado',
        'guia',
        'paquete',
        'fecha_envio_programada',
        'orden'
    )
    list_filter = (
        'user',
        'estado',
        'guia',
        'paquete',
        'fecha_envio_programada',
        'orden'
    )
    search_fields = [
        'user__email',
        'estado',
        'guia',
        'paquete__nombre',
        'fecha_envio_programada',
        'orden__lapTransactionState'
    ]
    ordering = ['fecha_envio_programada', ]
