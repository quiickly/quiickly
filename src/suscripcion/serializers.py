# -*- coding: utf-8 -*-
from rest_framework import serializers
from django.contrib.auth import get_user_model
from .models import *
User = get_user_model()


class PaqueteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Paquete
        fields = (
            'id', 'nombre', 'productos', 'precio', 'cantidad', 'descripcion'
        )
