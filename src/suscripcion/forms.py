from __future__ import unicode_literals
from django import forms
from django.contrib.auth import get_user_model
from .models import *
from profiles.models import Profile
import autocomplete_light
from cities_light.models import City

User = get_user_model()


class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['email', 'name', 'password']
        widgets = {
            'password': forms.PasswordInput(),
        }


class UserUpdateForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ['email', 'name']


class UserProfile(forms.ModelForm):

    class Meta:
        model = Profile
        fields = ['telefono']


class SuscripcionForm(forms.ModelForm):

    class Meta:
        model = Suscripcion
        fields = ['fecha_entrega']


class DireccionOndemandForm(forms.ModelForm):
    ciudad = forms.ModelChoiceField(
        City.objects.all(),
        widget=autocomplete_light.ChoiceWidget('CityAutocomplete')
    )

    class Meta:
        model = Direccion
        exclude = ['user', ]
        fields = '__all__'
        widgets = {'ciudad': forms.TextInput(attrs={'class': 'name'})}
