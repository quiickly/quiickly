from django.conf.urls import url
from . import views

urlpatterns = [
    # url(r'^$', views.OndemandView.as_view(), name='ondemand_index'),
    url(r'^tienda/$', 'web.views.Ondemand', name='ondemand_index'),
    url(r'^orden/$', views.RespuestaView.as_view(), name='ondemand_respuesta'),
    url(r'^pedido/$', views.PedidoView.as_view(), name='ondemand_pedido'),
    url(r'^confirmacion/$', views.ConfirmacionView.as_view(), name='ondemand_confirmacion'),
    url(r'^cuenta/$', views.CuentaView.as_view(), name='cuenta'),
    url(r'^suscripcion/$', views.CuentaView.as_view(), name='suscripcion'),
    url(r'^tienda/utils/productos/$', 'web.views.productosOndemand', name='productos_ajax'),
    url(r'^tienda/utils/paquetes/(?P<pk>[0-9]+)/$', 'web.views.PaquetesOndemand', name="paquetes_ajax"),
]
