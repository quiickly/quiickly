# -*- coding: utf-8 -*-
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from rest_framework.response import Response
from django.views.generic.base import TemplateView, View
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from suscripcion.serializers import PaqueteSerializer
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from rest_framework import permissions
from django.contrib import auth
from django.contrib import messages
from payu.models import Orden
from payu.utilspayu import *
from dashboard.models import Producto
from dashboard.serializers import ProductoOndemandSerializer
from suscripcion.forms import UserForm, UserProfile, DireccionOndemandForm, SuscripcionForm, UserUpdateForm
from suscripcion.models import Paquete
from .forms import *
from uuid import uuid4
from hashlib import md5
from django.views.decorators.csrf import csrf_exempt
import datetime
from suscripcion.models import Envio
from payu import configpayu
from payu.models import Configuracion
import logging
logger = logging.getLogger("project")


def Ondemand(request):

    payuVar = Configuracion.get_solo()
    if request.method == 'GET':
        # condiciones
        # ** si el usuario esta autenticado, pero no
        # ** tiene suscripcion (registro desde la app).
        # *
        # ** si el usuario esta autenticado y tiene una suscripcion
        # ** se debe mostrar el paquete seleccionado.

        if request.user.is_authenticated():
            try:
                request.user.suscripcion
                form = UserUpdateForm(instance=request.user)
                form_profile = UserProfile(instance=request.user.profile)
                form_envio = DireccionOndemandForm(
                    instance=request.user.suscripcion.direccion
                )
                suscripcion_form = SuscripcionForm(
                    instance=request.user.suscripcion
                )

            except ObjectDoesNotExist:
                form = UserUpdateForm(instance=request.user)
                form_profile = UserProfile(instance=request.user.profile)
                form_envio = DireccionOndemandForm()
                suscripcion_form = SuscripcionForm()

        else:
            form = UserForm()
            form_profile = UserProfile()
            form_envio = DireccionOndemandForm()
            suscripcion_form = SuscripcionForm()
        # TO-DO: hacer que el producto relacionado con la suscripcion,
        # aparezca seleccionado, al igual que el paquete.

        productos = Producto.objects.all().exclude(distribucion="app")
        producto_recomendado = productos.get(id=1)
        planes = Paquete.objects.filter(productos=producto_recomendado)
        return render(
            request,
            "web/ondemand.html",
            {
                "planes": planes,
                "suscripcion_form": suscripcion_form,
                "user_form": form,
                "profile_form": form_profile,
                "productos": productos,
                "envio_form": form_envio,
                "p_recomendado": producto_recomendado
            }
        )

    if request.method == 'POST':
        # cuando los formularios estan correctos -- OK
        # cuando los formularios estan incorrectos -- ok

        productos = Producto.objects.all().exclude(distribucion="app")
        producto_recomendado = productos.get(id=1)
        planes = Paquete.objects.filter(productos=producto_recomendado)

        if request.user.is_authenticated():
            try:
                request.user.suscripcion

                form = UserUpdateForm(request.POST, instance=request.user)
                form_profile = UserProfile(
                    request.POST,
                    instance=request.user.profile
                )
                form_envio = DireccionOndemandForm(
                    request.POST,
                    instance=request.user.suscripcion.direccion
                )
                suscripcion_form = SuscripcionForm(
                    request.POST,
                    instance=request.user.suscripcion
                )
            except ObjectDoesNotExist:
                form = UserUpdateForm(request.POST, instance=request.user)
                form_profile = UserProfile(
                    request.POST,
                    instance=request.user.profile
                )
                form_envio = DireccionOndemandForm(request.POST)
                suscripcion_form = SuscripcionForm(request.POST)

        else:
            form = UserForm(request.POST)
            form_envio = DireccionOndemandForm(request.POST)
            form_profile = UserProfile(request.POST)
            suscripcion_form = SuscripcionForm(request.POST)

        if not 'paquete' in request.POST:
            return render(
                request,
                "web/ondemand.html",
                {
                    "user_form": form,
                    "profile_form": form_profile,
                    "envio_form": form_envio,
                    "suscripcion_form": suscripcion_form,
                    "productos": productos,
                    "planes": planes,
                    "p_recomendado": producto_recomendado,
                    "msg": "Debes seleccionar un paquete.",
                }
            )

        if form.is_valid() and form_envio.is_valid() and form_profile.is_valid() and suscripcion_form.is_valid():
            # sino esta logueado, hay que crear
            # la suscripcion y guardar los formularios
            # crear usuario
            user = form.save(commit=False)
            # guardar pass sin encryptar, para la autentificacion
            if request.POST.get('password'):
                pass_text = user.password
                # encryptar la pass
                user.set_password(user.password)
            user.save()
            # crear la direccion
            direccion = form_envio.save(commit=False)
            direccion.user = user
            direccion.save()
            # obtener paquete
            paquete = Paquete.objects.get(id=request.POST.get('paquete'))
            # crear suscripcion
            suscripcion = suscripcion_form.save(commit=False)
            suscripcion.paquete = paquete
            suscripcion.user = user
            suscripcion.direccion = direccion
            suscripcion.valor = paquete.precio
            suscripcion.save()
            # actualizar perfil, el perfil se crea automaticamente
            # cuando se crea un usuario
            form_profile = UserProfile(request.POST, instance=user.profile)
            form_profile.save()

            if not request.user.is_authenticated():
                # autenticar
                user_auth = auth.authenticate(
                    email=user.email,
                    password=pass_text
                )
                auth.login(request, user_auth)

            # crear registro de la orden
            referenceCode = uuid4().hex
            orden = Orden()
            orden.referenceCode = referenceCode
            orden.user = user
            orden.merchantId = payuVar.merchant_id_test if payuVar.test else payuVar.merchant_id
            # “ApiKey~merchantId~referenceCode~amount~currency”.
            orden.signature = generate_hash_payu(
                payuVar.api_key_test if payuVar.test else payuVar.api_key,
                payuVar.merchant_id_test if payuVar.test else payuVar.merchant_id,
                referenceCode,
                paquete.precio,
                payuVar.currency)
            orden.TX_VALUE = paquete.precio
            orden.TX_TAX = 0
            orden.buyerEmail = user.email
            orden.currency = payuVar.currency
            orden.save()
            return HttpResponse(
                """
                  <html>
                      <head><title>Redirecting...</title></head>
                      <body>
                      <form action='%s' method='post' name="payu">
                        <input name="merchantId" type="hidden" value="%s">
                        <input name="accountId" type="hidden" value="%s">
                        <input name="description" type="hidden" value="%s">
                        <input name="referenceCode" type="hidden" value="%s">
                        <input name="amount" type="hidden" value="%s">
                        <input name="tax" type="hidden" value="0">
                        <input name="taxReturnBase" type="hidden" value="0">
                        <input name="currency" type="hidden"  value="%s">
                        <input name="signature" type="hidden" value="%s">
                        <input name="test" type="hidden" value="%s">
                        <input name="buyerEmail" type="hidden" value="%s">
                        <input name="responseUrl" type="hidden" value="%s">
                        <input name="confirmationUrl" type="hidden" value="%s">
                        <input name="Submit" type="submit" value="Enviar" style="display:none;">
                      </form>
                      </body>
                      <script language='javascript'>
                      window.onload = function(){
                       document.forms['payu'].submit()
                      }
                      </script>
                  </html>

              """ % (payuVar.url_test if payuVar.test else payuVar.url_produccion,
                     payuVar.merchant_id_test if payuVar.test else payuVar.merchant_id,
                     payuVar.account_id_test if payuVar.test else payuVar.account_id,
                     paquete.descripcion,
                     referenceCode,
                     paquete.precio,
                     payuVar.currency,
                     orden.signature,
                     '1' if payuVar.test else '0',
                     orden.buyerEmail,
                     payuVar.url_success_test if payuVar.test else payuVar.url_success_prod,
                     payuVar.url_confirmation_test if payuVar.test else payuVar.url_confirmation_prod,
                     )
            )

        else:
            return render(
                request,
                "web/ondemand.html",
                {
                    "user_form": form,
                    "planes": planes,
                    "profile_form": form_profile,
                    "envio_form": form_envio,
                    "suscripcion_form": suscripcion_form,
                    "productos": productos,
                    "p_recomendado": producto_recomendado,
                }
            )


class OndemandView(TemplateView):

    template_name = "web/ondemand.html"

    def get_context_data(self, **kwargs):
        context = super(OndemandView, self).get_context_data(**kwargs)
        return context


class RespuestaView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(RespuestaView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(RespuestaView, self).get_context_data(**kwargs)
        return context

    def get(self, request, *args, **kwargs):
        rg = request.GET.get
        payuVar = Configuracion.get_solo()
        orden = get_object_or_404(Orden, referenceCode=rg('referenceCode'))
        ApiKey = payuVar.api_key_test if payuVar.test else payuVar.api_key
        merchant_id = rg('merchantId')
        referenceCode = rg('referenceCode')
        TX_VALUE = rg('TX_VALUE')
        # TX_TAX = rg('TX_TAX')
        New_value = payu_number_format(TX_VALUE if TX_VALUE else 1, 1)
        currency = rg('currency')
        transactionState = rg('transactionState')
        firma_cadena = ApiKey + '~' + merchant_id + '~' + referenceCode + '~' + str(New_value) + '~' + currency +'~' + transactionState
        firmacreada = md5(firma_cadena).hexdigest().lower()
        firma = rg('signature')
        reference_pol = rg('reference_pol')
        # cus = rg('cus')
        description = rg('description')
        # pseBank = rg('pseBank')
        lapPaymentMethod = rg('lapPaymentMethod')
        transactionId = rg('transactionId')
        lapTransactionState = rg('lapTransactionState')
        # lapResponseCode = rg('lapResponseCode')
        message = rg('message')
        # processingDate = rg('processingDate')
        valido = True if firmacreada == firma else False
        data = {
            "referenceCode": referenceCode,
            "TX_VALUE": TX_VALUE,
            "transactionState": transactionState,
            "reference_pol": reference_pol,
            "description": description,
            "lapPaymentMethod": lapPaymentMethod,
            "transactionId": transactionId,
            "lapTransactionState": lapTransactionState,
            "message": message,
            "valido": valido,
            "currency": currency,
        }

        return render(
            request, "web/respuesta.html", {"data": data, "orden": orden}
        )


class CuentaView(View):

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CuentaView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        form = UserFormWeb(instance=request.user)
        profile_form = ProfileFormWeb(instance=request.user.profile)
        return render(
            request, "web/cuenta_usuario.html",
            {"user_form": form, "profile_form": profile_form}
        )

    def post(self, request, *args, **kwargs):
        form = UserFormWeb(request.POST, instance=request.user)
        profile_form = ProfileFormWeb(
            request.POST,
            request.FILES,
            instance=request.user.profile
        )
        if form.is_valid() and profile_form.is_valid():
            form.save()
            profile_form.save()
            messages.success(self.request, 'Los datos fueron actualizados.')
            # creo los formularios nuevamente para que se refleje la imagen.
            form_n = UserFormWeb(instance=request.user)
            p_form = ProfileFormWeb(instance=request.user.profile)
            return render(
                request, "web/cuenta_usuario.html",
                {"user_form": form_n, "profile_form": p_form}
            )
        else:
            return render(
                request, "web/cuenta_usuario.html",
                {"user_form": form, "profile_form": profile_form}
            )


class ConfirmacionView(View):

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ConfirmacionView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return render(
            request, "web/confirmacion.html",
            {"hola": "hola"}
        )

    def post(self, request, *args, **kwargs):
        payuVar = Configuracion.get_solo()
        rp = request.POST.get
        print "##### Request.post pagina de confirmacion ######"
        print request.POST
        print "##### fin #####"
        # confirmar los datos con el sign, antes de guardar
        # generar hash
        # "ApiKey~merchant_id~reference_sale~new_value~currency~state_pol".
        ApiKey = payuVar.api_key_test if payuVar.test else payuVar.api_key
        logger.info('Entrada a confirmacion: orden {} request.POST'.format(rp('reference_sale')))
        new_value = payu_number_format_confirmation(float(str(rp('value'))))
        sign_gen = generate_hash_confirmation_payu(
            ApiKey,
            rp('merchant_id'),
            rp('reference_sale'),
            new_value,
            rp('currency'),
            rp('state_pol')
        )
        logger.info('Datos para hash: ApiKey: {}, mId: {}, rsale: {}, new_value: {}, currency: {}, state_pol: {}'.format(ApiKey, rp('merchant_id'), rp('reference_sale'), new_value, rp('currency'), rp('state_pol')))
        sign = rp('sign')
        logger.info('SIGN recibida: {}'.format(sign))
        logger.info('SIGN generada: {}'.format(sign_gen))
        if sign_gen != sign:
            logger.info('Las credenciales son invalidas')
            return HttpResponse("las credenciales no coinciden", status=400)
        orden = get_object_or_404(Orden, referenceCode=rp('reference_sale'))
        logger.info('Entrada a confirmacion: orden {}'.format(orden.referenceCode))
        if rp('state_pol') == '6':
            orden.lapTransactionState = "DECLINED"
        elif rp('state_pol') == '5':
            orden.lapTransactionState = "EXPIRED"
        elif rp('state_pol') == '4':
            orden.lapTransactionState = "APPROVED"
        # orden.lapTransactionState = rp('lapTransactionState')
        orden.transactionState = rp('transactionState')
        orden.lapResponseCode = rp('lapResponseCode')
        orden.reference_pol = rp('reference_pol')
        orden.lapPaymentMethod = rp('payment_method_name')
        orden.message = rp('response_message_pol')
        orden.transactionId = rp('transaction_id')
        if 'date' in request.POST:
            orden.processingDate = datetime.datetime.strptime(
                str(rp('date')),
                "%Y.%m.%d %H:%M:%S"
            )
        orden.save()
        return HttpResponse(status=200)


class PedidoView(View):

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(PedidoView, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        envios = Envio.objects.all().filter(user=request.user)
        payuVar = Configuracion.get_solo()
        print payuVar.api_key_test if payuVar.test else payuVar.api_key
        print payuVar.merchant_id_test if payuVar.test else payuVar.merchant_id
        print payuVar.account_id_test if payuVar.test else payuVar.account_id
        print payuVar.api_login
        print payuVar.currency
        print payuVar.public_key
        print payuVar.url_test if payuVar.test else payuVar.url_produccion
        print payuVar.url_success_test if payuVar.test else payuVar.url_success_prod
        print payuVar.url_confirmation_test if payuVar.test else payuVar.url_confirmation_prod
        print '1' if payuVar.test else '0'
        return render(
            request, "web/pedido.html",
            {"envios": envios}
        )


@api_view(['GET'])
@permission_classes((permissions.AllowAny, ))
def productosOndemand(request):
    """
    Productos Ondemand - Ajax.
    """

    if request.method == 'GET':
        queryset = Producto.objects.all().exclude(distribucion='app')
        serializer = ProductoOndemandSerializer(queryset, many=True)
        return Response(serializer.data)


@api_view(['GET'])
@permission_classes((permissions.AllowAny, ))
def PaquetesOndemand(request, pk):
    """
    Lista de paquetes segun el producto.
    """
    try:
        producto = Producto.objects.get(pk=pk)
    except Producto.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        paquetes = Paquete.objects.all().filter(productos=producto)
        serializer = PaqueteSerializer(paquetes, many=True)
        return Response(serializer.data)
