from __future__ import unicode_literals
from django import forms
from django.contrib.auth import get_user_model
from profiles.models import Profile
from .models import *

User = get_user_model()


class UserFormWeb(forms.ModelForm):

    class Meta:
        model = User
        fields = ['email', 'name']


class ProfileFormWeb(forms.ModelForm):

    class Meta:
        model = Profile
        fields = ['picture', 'telefono']
