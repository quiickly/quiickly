# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse

def menuWeb(request):
    menu = {'menuWeb': [
        {'name': 'Tienda', 'url': '/tienda', 'link': reverse('web:ondemand_index')
        },
        {'name': 'Mi Pedido', 'url': '/pedido', 'link': reverse('web:ondemand_pedido')
        },
        {'name': 'Cuenta', 'url': '/cuenta', 'link': reverse('web:cuenta')
        },
        {'name': 'Salir', 'link': reverse('accounts:logout')
        },


    ]}
    for men in menu['menuWeb']:
        if 'url' in men:
            if men['url'] in request.path:
                men['active'] = True
    return menu
