from hashlib import md5
from django.conf import settings
KEYS = ('key', 'txnid', 'amount', 'productinfo', 'firstname', 'email',
        'udf1', 'udf2', 'udf3', 'udf4', 'udf5',  'udf6',  'udf7', 'udf8',
        'udf9',  'udf10')


def generate_hash(data):
    # parametros
    # “ApiKey~merchantId~referenceCode~amount~currency”.
    hash = md5('6u39nqhq8ftd0hlvnjfs66eh8c~500238~TestPayU~3~USD')
    return hash.hexdigest().lower()

def verify_hash(data, SALT):
    keys.reverse()
    hash = sha512(settings.PAYU_INFO.get('merchant_salt'))
    hash.update("%s%s" % ('|', str(data.get('status', ''))))
    for key in KEYS:
        hash.update("%s%s" % ('|', str(data.get(key, ''))))
    return (hash.hexdigest().lower() == data.get('hash'))
